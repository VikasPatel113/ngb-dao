package com.mppkvvcl.ngbdao.interfaces;

/**
 * Created by RUPALI on 9/16/2017.
 */
public interface ConfiguratorDAOInterface<T> extends DAOInterface<T> {
    public T getByCode(String code);
}
