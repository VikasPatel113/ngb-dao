package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
public interface MeterCTRMappingDAOInterface<T> extends DAOInterface<T> {

    public List<T> getByMeterIdentifier(String meterIdentifier);

    public List<T> getByMeterIdentifierAndMappingStatus(String meterIdentifier, String mappingStatus);

    public List<T> getByCtrIdentifierAndMappingStatus(String ctrIdentifier, String mappingStatus);
}
