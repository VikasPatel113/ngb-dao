package com.mppkvvcl.ngbdao.interfaces;

import com.mppkvvcl.ngbinterface.interfaces.MeterReaderInformationInterface;

/**
 * Created by ASHISH on 12/14/2017.
 */
public interface MeterReaderInformationDAOInterface<T> extends DAOInterface<T> {

    public MeterReaderInformationInterface getByReadingDiaryNoId(long readingDiaryNoId);
}
