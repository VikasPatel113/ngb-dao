package com.mppkvvcl.ngbdao.interfaces;
import java.util.Date;
import java.util.List;

/**
 * Created by WORK6 on 9/16/2017.
 */
public interface SeasonalConnectionInformationDAOInterface<T> extends DAOInterface<T>{

    public List<T> getByConsumerNo(String consumerNo);

    public List<T> getBySeasonStartDateAndSeasonEndDate(Date givenDate);

    public List<T> getBetweenSeasonStartDateAndSeasonEndDateAndConsumerNo(Date givenDate, String consumerNo);

    public List<T> getByViolationMonth(String violationMonth);

    public T getTopByConsumerNoOrderByIdDesc(String consumerNo);


}
