package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

/**
 * Created by ANSHIKA on 15-09-2017.
 */
public interface AdjustmentHierarchyDAOInterface<T> extends DAOInterface<T>{

    public List<T> getByPriorityBetweenUserPriorityAndAdjustmentPriority(int userPriority, int adjustmentPriority);
}
