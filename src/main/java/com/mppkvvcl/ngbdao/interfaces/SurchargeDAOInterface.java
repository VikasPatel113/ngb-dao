package com.mppkvvcl.ngbdao.interfaces;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface SurchargeDAOInterface<T> extends DAOInterface<T>{

    public List<T> getByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode);

    public List<T> getByTariffIdAndSubcategoryCodeAndOutstandingAmountGreaterThanEqual(long tariffId, long subcategoryCode, BigDecimal outstandingAmount);

}
