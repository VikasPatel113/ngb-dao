package com.mppkvvcl.ngbdao.interfaces;

import com.mppkvvcl.ngbinterface.interfaces.LoadDetailInterface;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by SHIVANHSU on 16-09-2017.
 */
public interface LoadDetailDAOInterface<T> extends  DAOInterface<T>{
    public T getTopByConsumerNoOrderByIdDesc(String consumerNo);

    public List<T> getByConsumerNo(final String consumerNo);

    public List<T> getByConsumerNoAndContractDemandNotNullOrderByIdAsc(String consumerNo);
}
