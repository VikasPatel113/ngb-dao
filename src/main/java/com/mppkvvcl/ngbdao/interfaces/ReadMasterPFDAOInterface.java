package com.mppkvvcl.ngbdao.interfaces;
/**
 * Created by RUPALI on 9/16/2017.
 */
public interface ReadMasterPFDAOInterface<T> extends DAOInterface<T>  {
    public T getByReadMasterId(Long readMasterId);
}
