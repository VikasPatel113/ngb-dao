package com.mppkvvcl.ngbdao.interfaces;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface XrayConnectionInformationDAOInterface<T> extends DAOInterface<T>{

    public List<T> getByConsumerNo(String consumerNo);

    public List<T> getByConsumerNoAndStatus(String consumerNo, String status);

    public List<T> getByXrayLoadBetweenAndStatus(BigDecimal startLoad, BigDecimal endLoad, String status);

    public List<T> getByStatus(String status);

    public List<T> getByNoOfDentalXrayMachineBetween(Long start, Long end);

    public List<T> getByNoOfSinglePhaseXrayMachineBetween(Long start, Long end);
}
