package com.mppkvvcl.ngbdao.interfaces;

import com.mppkvvcl.ngbinterface.interfaces.AgricultureUnitInterface;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
public interface AgricultureUnitDAOInterface<T> extends DAOInterface<T> {

    public T getBySubcategoryCodeAndBillMonth(long subcategoryCode,String billMonth);

    public T getByTariffIdAndAlternateSubcategoryCode(long tariffId, long subcategoryCode) ;
}
