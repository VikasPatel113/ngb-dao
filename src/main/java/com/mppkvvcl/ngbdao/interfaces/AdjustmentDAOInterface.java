package com.mppkvvcl.ngbdao.interfaces;

import com.mppkvvcl.ngbinterface.interfaces.AdjustmentInterface;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
public interface AdjustmentDAOInterface<T> extends DAOInterface<T> {

    public T getByConsumerNoAndPostingBillMonth(String consumerNo , String postingBillMonth);

    public List<T> getByConsumerNoAndPostedAndDeleted(String consumerNo , boolean posted , boolean deleted);

    public List<T> getByConsumerNoAndPostedAndDeletedAndApprovalStatus(String consumerNo , boolean posted , boolean deleted,String approvalStatus);

    public List<T> getByConsumerNoAndApprovalStatus(String consumerNo ,String approvalStatus);

    public List<T> getByLocationCodeAndPostedAndDeleted(String locationCode, boolean posted, boolean deleted);

    public List<T> getByConsumerNoAndCodeAndPostedAndDeleted(String consumerNo, int code, boolean posted, boolean deleted);

    public List<T> add(List<T> list);

    public List<T> getByConsumerNoAndPostingBillMonthAndPostedAndDeleted(String consumerNo, String postingBillMonth, boolean posted, boolean deleted);

    //count methods
    public long getCountByConsumerNo(String consumerNo);

    public long getCountByConsumerNoAndPostedAndDeleted(String consumerNo , boolean posted , boolean deleted);

    public long getCountByConsumerNoAndPostedAndDeletedAndApprovalStatus(String consumerNo , boolean posted , boolean deleted,String approvalStatus);

    public long getCountByConsumerNoAndApprovalStatus(String consumerNo ,String approvalStatus);

    public long getCountByLocationCodeAndPostedAndDeleted(String locationCode, boolean posted, boolean deleted);

    public long getCountByConsumerNoAndCodeAndPostedAndDeleted(String consumerNo, int code, boolean posted, boolean deleted);

    //Paginated methods
    public List<T> getByConsumerNo(String consumerNo, Pageable pageable);

    public List<T> getByConsumerNoOrderByPostingBillMonthDescending(String consumerNo, Pageable pageable);

    public List<T> getByConsumerNoOrderByPostingBillMonthAscending(String consumerNo, Pageable pageable);

    public List<T> getByConsumerNoAndPostedAndDeleted(String consumerNo , boolean posted , boolean deleted,Pageable pageable);

    public List<T> getByConsumerNoAndPostedAndDeletedOrderByPostingBillMonthDescending(String consumerNo , boolean posted , boolean deleted,Pageable pageable);

    public List<T> getByConsumerNoAndPostedAndDeletedOrderByPostingBillMonthAscending(String consumerNo , boolean posted , boolean deleted,Pageable pageable);

    public List<T> getByConsumerNoAndPostedAndDeletedAndApprovalStatus(String consumerNo , boolean posted , boolean deleted,String approvalStatus,Pageable pageable);

    public List<T> getByConsumerNoAndPostedAndDeletedAndApprovalStatusOrderByPostingBillMonthDescending(String consumerNo , boolean posted , boolean deleted,String approvalStatus,Pageable pageable);

    public List<T> getByConsumerNoAndPostedAndDeletedAndApprovalStatusOrderByPostingBillMonthAscending(String consumerNo , boolean posted , boolean deleted,String approvalStatus,Pageable pageable);

    public List<T> getByConsumerNoAndApprovalStatus(String consumerNo ,String approvalStatus,Pageable pageable);

    public List<T> getByConsumerNoAndApprovalStatusOrderByPostingBillMonthDescending(String consumerNo ,String approvalStatus,Pageable pageable);

    public List<T> getByConsumerNoAndApprovalStatusOrderByPostingBillMonthAscending(String consumerNo ,String approvalStatus,Pageable pageable);

    public List<T> getByLocationCodeAndPostedAndDeleted(String locationCode, boolean posted, boolean deleted,Pageable pageable);

    public List<T> getByLocationCodeAndPostedAndDeletedOrderByPostingBillMonthDescending(String locationCode, boolean posted, boolean deleted,Pageable pageable);

    public List<T> getByLocationCodeAndPostedAndDeletedOrderByPostingBillMonthAscending(String locationCode, boolean posted, boolean deleted,Pageable pageable);

    public List<T> getByConsumerNoAndCodeAndPostedAndDeleted(String consumerNo, int code, boolean posted, boolean deleted,Pageable pageable);

    public List<T> getByConsumerNoAndCodeAndPostedAndDeletedOrderByPostingBillMonthDescending(String consumerNo, int code, boolean posted, boolean deleted,Pageable pageable);

    public List<T> getByConsumerNoAndCodeAndPostedAndDeletedOrderByPostingBillMonthAscending(String consumerNo, int code, boolean posted, boolean deleted,Pageable pageable);

    public List<T> getByConsumerNoAndPostingBillMonthAndPostedAndDeleted(String consumerNo, String postingBillMonth, boolean posted, boolean deleted,Pageable pageable);
}

