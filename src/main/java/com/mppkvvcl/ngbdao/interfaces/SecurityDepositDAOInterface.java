package com.mppkvvcl.ngbdao.interfaces;

import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 11/17/2017.
 */
public interface SecurityDepositDAOInterface<T> extends DAOInterface<T> {
    public List<T> getByConsumerNoOrderByIdDesc(String consumerNo);
    public List<T> add(List<T> list);
    public List<T> getByConsumerNoAndEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(String consumerNo,Date effectiveStartDate,Date effectiveEndDate);
    public T getTopByConsumerNoOrderByIdDesc(String consumerNo);
}
