package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
public interface PurposeOfInstallationDAOInterface<T> extends DAOInterface<T> {
    public List<T> getByTariffCode(String tariffCode);
    public T getByPurposeOfInstallation(String purposeOfInstallation);
}
