package com.mppkvvcl.ngbdao.interfaces;

import java.util.Date;
import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
public interface ConsumerConnectionAreaInformationDAOInterface<T> extends DAOInterface<T> {
    public T getByConsumerNo(String consumerNumber);
    public List<T> getByDtrName(String dtrName);
    public List<T> getByFeederName(String feederName);
    public List<T> getByFeederNameAndDtrName(String feederName,String dtrName);
    public List<T> getByGroupNo(String groupNo);
    public List<T> getByGroupNoAndReadingDiaryNo(String groupNo, String readingDiaryNo);
    public List<T> getBySurveyDate(Date surveyDate);
    public List<T> getBySurveyDateBetween( Date startSurveyDate, Date endSurveyDate);
    public List<T> getByFeederNameAndDtrNameAndPoleNo(String feederName, String dtrName,String poleNo);
    public List<T> getByFeederNameAndAreaStatus(String feederName, String areaStatus);

    public long countByGroupNo(String groupNo);

    public long countByGroupNoAndReadingDiaryNo(String groupNo,String readingDiaryNo);
}
