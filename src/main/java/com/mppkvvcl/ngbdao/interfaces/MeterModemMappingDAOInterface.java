package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
public interface MeterModemMappingDAOInterface<T> extends DAOInterface<T>{

    public List<T> getByMeterIdentifier(String meterIdentifier);

    public List<T> getByMeterIdentifierAndStatus(String meterIdentifier, String status);

}
