package com.mppkvvcl.ngbdao.interfaces;

public interface BillTypeCodeDAOInterface<T> extends DAOInterface<T> {
    public T getByCode(String code);
}
