package com.mppkvvcl.ngbdao.interfaces;
import com.mppkvvcl.ngbinterface.interfaces.TariffInterface;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
public interface TariffDAOInterface<T> extends DAOInterface<T>{

    public List<T> getByTariffCategoryAndEffectiveDate(String tariffCategory, String date);

    public List<T> getByTariffCategoryAndMeteringStatusAndEffectiveDate(String tariffCategory, String meteringStatus, String date);

    public List<T> getByTariffCategoryAndConnectionTypeAndEffectiveDate(String tariffCategory, String connectionType, String date);

    public List<T> getByTariffCategoryAndMeteringStatusAndConnectionTypeAndEffectiveDate(String tariffCategory, String meteringStatus, String connectionType, String date);

    public T getById(long id);

    public List<T> getByTariffCategoryAndMeteringStatusAndConnectionTypeAndTariffTypeAndEffectiveDate(String tariffCategory,  String meteringStatus,  String connectionType,  String tariffType, String date);

    public T getByTariffCodeAndEffectiveDate(String tariffCode, String effectiveDate);

    public T getTopByTariffCodeOrderByIdDesc(String tariffCode);

    public List<? extends T> getAll();

    public List<T> getByTariffCodeAndEffectiveStartDateAndEffectiveEndDate(String tariffCode,String effectiveStartDate,String effectiveEndDate);
}
