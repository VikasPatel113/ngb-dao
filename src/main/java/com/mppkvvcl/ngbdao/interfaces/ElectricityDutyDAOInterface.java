package com.mppkvvcl.ngbdao.interfaces;

import java.util.Date;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface ElectricityDutyDAOInterface<T> extends DAOInterface<T> {

    public List<T> getBySubCategoryCodeAndDate(Long subCategoryCode, Date givenDate);

}
