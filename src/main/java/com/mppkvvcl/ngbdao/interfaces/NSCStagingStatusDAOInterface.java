package com.mppkvvcl.ngbdao.interfaces;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingStatusInterface;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
public interface NSCStagingStatusDAOInterface<T> extends DAOInterface<T> {

    public T getByNscStagingId(long nscStagingId);

    public List<T> getByStatus(String status);

    public T getByConsumerNo(String consumerNo);

    public T getByPendingStatusAndMeterIdentifier(String status,String meterIdentifier);

    public List<T> getByLocationCodeAndStatus(String locationCode,String status);

    public List<T> getByLocationCode(String locationCode);

    public List<T> getByGroupNoAndStatus(String groupNo,String status);

    public List<T> getByGroupNo(String groupNo);

    //count methods coded by nitish
    public long getCountByLocationCode(String locationCode);

    public long getCountByLocationCodeAndStatus(String locationCode,String status);

    public long getCountByGroupNo(String groupNo);

    public long getCountByGroupNoAndStatus(String groupNo,String status);

    //Pageable methods coded by nitish
    public List<T> getByLocationCode(String locationCode, Pageable pageable);

    public List<T> getByLocationCodeAndStatus(String locationCode,String status, Pageable pageable);

    public List<T> getByGroupNo(String groupNo,Pageable pageable);

    public List<T> getByGroupNoAndStatus(String groupNo,String status,Pageable pageable);

    public T getByStatusAndBPlNo(String status,String bplNo);

    public T getByStatusAndEmployeeNo(String status,String employeeNo);

}
