package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface DivisionDAOInterface<T> extends DAOInterface<T>{
    public List<T> getByCircleId(long circleId);
    public List<? extends T> getAll();
}
