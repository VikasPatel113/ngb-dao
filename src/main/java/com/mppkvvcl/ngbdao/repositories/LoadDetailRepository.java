package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.LoadDetailInterface;
import com.mppkvvcl.ngbentity.beans.LoadDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface LoadDetailRepository extends JpaRepository<LoadDetail,Long>{
    public LoadDetailInterface findTopByConsumerNoOrderByIdDesc(String consumerNo);
    public LoadDetailInterface save(LoadDetailInterface loadDetailInterface);
    public List<LoadDetailInterface> findByConsumerNoOrderByIdDesc(String consumerNo);

    public List<LoadDetailInterface> findByConsumerNoAndContractDemandNotNullOrderByIdAsc(String consumerNo);
}
