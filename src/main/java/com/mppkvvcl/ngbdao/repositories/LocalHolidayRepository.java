package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.LocalHolidayInterface;
import com.mppkvvcl.ngbentity.beans.LocalHoliday;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * Created by PREETESH on 9/11/2017.
 */
@Repository
public interface LocalHolidayRepository extends JpaRepository<LocalHoliday,Long> {
    public LocalHolidayInterface findByLocationCodeAndDate(String locationCode, Date date);
}
