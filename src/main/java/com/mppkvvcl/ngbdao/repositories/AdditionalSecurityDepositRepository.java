package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.AdditionalSecurityDeposit;
import com.mppkvvcl.ngbinterface.interfaces.AdditionalSecurityDepositInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PREETESH on 11/17/2017.
 */
@Repository
public interface AdditionalSecurityDepositRepository extends JpaRepository<AdditionalSecurityDeposit, Long> {

    public AdditionalSecurityDepositInterface save(AdditionalSecurityDepositInterface additionalSecurityDepositInterface);
    public List<AdditionalSecurityDepositInterface> findByConsumerNo(String consumerNo);
    public AdditionalSecurityDepositInterface findByConsumerNoAndBillMonth(String consumerNo,String billMonth);


}
