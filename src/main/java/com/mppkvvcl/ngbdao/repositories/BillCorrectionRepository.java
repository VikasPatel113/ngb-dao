package com.mppkvvcl.ngbdao.repositories;


import com.mppkvvcl.ngbentity.beans.BillCorrection;
import com.mppkvvcl.ngbinterface.interfaces.BillCorrectionInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BillCorrectionRepository extends JpaRepository<BillCorrection, Long> {

    public BillCorrectionInterface save(BillCorrectionInterface billDifferenceInterface);

    public List<BillCorrectionInterface> findByConsumerNoAndBillMonthAndDeleted(String consumerNo, String billMonth, boolean deleted);
}
