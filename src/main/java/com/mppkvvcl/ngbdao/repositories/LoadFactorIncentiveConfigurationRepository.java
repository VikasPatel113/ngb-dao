package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.LoadFactorIncentiveConfiguration;
import com.mppkvvcl.ngbinterface.interfaces.LoadFactorIncentiveConfigurationInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 12/22/2017.
 */
@Repository
public interface LoadFactorIncentiveConfigurationRepository extends JpaRepository<LoadFactorIncentiveConfiguration,Long> {

@Query("from LoadFactorIncentiveConfiguration lfr where :lfValue >= lfr.startRange and :lfValue < lfr.endRange and :date >= lfr.startDate and :date < lfr.endDate")
public LoadFactorIncentiveConfigurationInterface findByLoadFactorValueAndDate(@Param("lfValue") BigDecimal lfValue, @Param("date") Date date);

public LoadFactorIncentiveConfigurationInterface save(LoadFactorIncentiveConfigurationInterface powerFactorInterface);

@Query("from LoadFactorIncentiveConfiguration lfr where :date >= lfr.startDate and :date < lfr.endDate")
public List<LoadFactorIncentiveConfigurationInterface> findByDate(@Param("date") Date date);

}
