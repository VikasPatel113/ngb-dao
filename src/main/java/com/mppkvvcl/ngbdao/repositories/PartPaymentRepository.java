package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.PartPaymentInterface;
import com.mppkvvcl.ngbentity.beans.PartPayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 8/2/2017.
 */
@Repository
public interface PartPaymentRepository extends JpaRepository<PartPayment, Long> {

    public List<PartPaymentInterface> findByConsumerNoAndAppliedOrderByIdDesc(String consumerNo, boolean applied);

    public PartPaymentInterface findTopByConsumerNoAndAppliedOrderByIdDesc(String consumerNo, boolean appliedTrue);

    public PartPaymentInterface findByConsumerNoAndAppliedAndUpdatedOn(String consumerNo, boolean appliedTrue, Date updatedOn);

    public List<PartPaymentInterface> findByConsumerNo(String consumerNo);

    public PartPaymentInterface save(PartPaymentInterface partPaymentInterface);
}
