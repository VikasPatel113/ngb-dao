package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.SECircleMappingInterface;
import com.mppkvvcl.ngbentity.beans.SECircleMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SECircleMappingRepository extends JpaRepository<SECircleMapping, Long> {

    public SECircleMappingInterface findByUserDetailUsername(String username);

    public SECircleMappingInterface findByCircleId(long circleId);

    public SECircleMappingInterface save(SECircleMappingInterface seCircleMappingInterface);
}
