package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.ReadMaster;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterInterface;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PREETESH on 6/30/2017.
 */
@Repository
public interface ReadMasterRepository extends JpaRepository<ReadMaster, Long>{
    public List<ReadMasterInterface> findByConsumerNo(String consumerNo);
    public ReadMasterInterface findTopByConsumerNoOrderByReadingDateDesc(String consumerNo);
    public ReadMasterInterface findTopByConsumerNoOrderByIdDesc(String consumerNo);
    public List<ReadMasterInterface> findByConsumerNoAndBillMonthAndReplacementFlagOrderByIdDesc(String consumerNo, String billMonth, String replacementFlag);
    public List<ReadMasterInterface> findByConsumerNoAndBillMonthOrderByIdAsc(String consumerNo, String billMonth);
    public ReadMasterInterface save(ReadMasterInterface readMasterInterface);
    public ReadMasterInterface findOne(long id);
    public ReadMasterInterface findByConsumerNoAndReplacementFlag(String consumerNo,String replacementFlag);
    public List<ReadMasterInterface> findByConsumerNoAndBillMonthAndReplacementFlagAndUsedOnBillOrderByIdDesc(String consumerNo, String billMonth, String replacementFlag, Boolean usedOnBill);

    public List<ReadMasterInterface> findTop2ByConsumerNoAndBillMonthOrderByIdDesc(String consumerNo, String billMonth);

    /**
     * Pageable Method returns list on the basis of pageable properties coded by nitish
     */
    @Query("from #{#entityName} rm where rm.consumerNo= :consumerNo order by to_date(rm.billMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') DESC")
    public List<ReadMasterInterface> findByConsumerNoOrderByBillMonthDescendingWithPageable(@Param("consumerNo") String consumerNo, Pageable pageable);

    @Query("from #{#entityName} rm where rm.consumerNo= :consumerNo order by to_date(rm.billMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') ASC")
    public List<ReadMasterInterface> findByConsumerNoOrderByBillMonthAscendingWithPageable(@Param("consumerNo") String consumerNo, Pageable pageable);

    public long countByConsumerNo(String consumerNo);

    public List<ReadMasterInterface> findByConsumerNoAndReplacementFlagAndUsedOnBillOrderByIdDesc(String consumerNo, String replacementFlag, Boolean usedOnBill);
}
