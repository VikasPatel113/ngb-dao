package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.GMCAccounting;
import com.mppkvvcl.ngbinterface.interfaces.GMCAccountingInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Created by PREETESH on 11/7/2017.
 */
@Repository
public interface GMCAccountingRepository extends JpaRepository<GMCAccounting, Long> {
    public GMCAccountingInterface findByConsumerNo(String consumerNo);
    public GMCAccountingInterface save(GMCAccountingInterface gmcAccounting);

    public GMCAccountingInterface findByConsumerNoAndCurrentBillMonth(String consumerNo, String currentBillMonth);
}
