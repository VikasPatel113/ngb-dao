package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.BankMasterInterface;
import com.mppkvvcl.ngbentity.beans.BankMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by RUPALI on 27-07-2017.
 */
@Repository
public interface BankMasterRepository extends JpaRepository<BankMaster, Long> {
   public BankMasterInterface save(BankMasterInterface bankMaster);


}
