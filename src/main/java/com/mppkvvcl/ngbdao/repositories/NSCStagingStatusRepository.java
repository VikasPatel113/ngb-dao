package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.NSCStagingStatusInterface;
import com.mppkvvcl.ngbentity.beans.NSCStagingStatus;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by SUMIT on 31-05-2017.
 * Repository Interface of NSCStagingStatus Entity
 * for various CRUD Application on Entity.
 */
@Repository
public interface NSCStagingStatusRepository extends JpaRepository<NSCStagingStatus,Long>{

    public NSCStagingStatusInterface findByNscStagingId(long nscStagingId);

    public List<NSCStagingStatusInterface> findByStatus(String status);

    public NSCStagingStatusInterface findByConsumerNo(String consumerNo);

    @Query("from nscstagingstatus nss,nscstaging ns where ns.id = nss.nscStagingId and nss.status = :status and ns.meterIdentifier = :meterIdentifier")
    public NSCStagingStatusInterface findByPendingStatusAndMeterIdentifier(@Param("status") String status,@Param("meterIdentifier") String meterIdentifier);

    public NSCStagingStatusInterface save(NSCStagingStatusInterface nscStagingStatusInterface);

    @Query("from nscstagingstatus nss,nscstaging ns where ns.id = nss.nscStagingId and nss.status = :status and ns.locationCode = :locationCode order by nss.nscStagingId desc")
    public List<NSCStagingStatusInterface> findByLocationCodeAndStatus(@Param("locationCode") String locationCode,@Param("status") String status);

    @Query("from nscstagingstatus nss,nscstaging ns where ns.id = nss.nscStagingId and ns.locationCode = :locationCode order by nss.nscStagingId desc")
    public List<NSCStagingStatusInterface> findByLocationCode(@Param("locationCode") String locationCode);

    @Query("from nscstagingstatus nss,nscstaging ns where ns.id = nss.nscStagingId and nss.status = :status and ns.groupNo = :groupNo order by nss.nscStagingId desc")
    public List<NSCStagingStatusInterface> findByGroupNoAndStatus(@Param("groupNo") String groupNo,@Param("status") String status);

    @Query("from nscstagingstatus nss,nscstaging ns where ns.id = nss.nscStagingId and ns.groupNo = :groupNo order by nss.nscStagingId desc")
    public List<NSCStagingStatusInterface> findByGroupNo(@Param("groupNo") String groupNo);

    //count methods coded by nitish
    @Query("select count(*) from nscstagingstatus nss,nscstaging ns where ns.id = nss.nscStagingId and ns.locationCode = :locationCode")
    public long countByLocationCode(@Param("locationCode") String locationCode);

    @Query("select count(*) from nscstagingstatus nss,nscstaging ns where ns.id = nss.nscStagingId and nss.status = :status and ns.locationCode = :locationCode")
    public long countByLocationCodeAndStatus(@Param("locationCode") String locationCode,@Param("status") String status);

    @Query("select count(*) from nscstagingstatus nss,nscstaging ns where ns.id = nss.nscStagingId and ns.groupNo = :groupNo")
    public long countByGroupNo(@Param("groupNo") String groupNo);

    @Query("select count(*) from nscstagingstatus nss,nscstaging ns where ns.id = nss.nscStagingId and nss.status = :status and ns.groupNo = :groupNo")
    public long countByGroupNoAndStatus(@Param("groupNo") String groupNo,@Param("status") String status);

    //Pageable methods coded by nitish
    @Query("from nscstagingstatus nss,nscstaging ns where ns.id = nss.nscStagingId and ns.locationCode = :locationCode")
    public List<NSCStagingStatusInterface> findByLocationCode(@Param("locationCode") String locationCode, Pageable pageable);

    @Query("from nscstagingstatus nss,nscstaging ns where ns.id = nss.nscStagingId and nss.status = :status and ns.locationCode = :locationCode")
    public List<NSCStagingStatusInterface> findByLocationCodeAndStatus(@Param("locationCode") String locationCode,@Param("status") String status,Pageable pageable);

    @Query("from nscstagingstatus nss,nscstaging ns where ns.id = nss.nscStagingId and ns.groupNo = :groupNo")
    public List<NSCStagingStatusInterface> findByGroupNo(@Param("groupNo") String groupNo,Pageable pageable);

    @Query("from nscstagingstatus nss,nscstaging ns where ns.id = nss.nscStagingId and nss.status = :status and ns.groupNo = :groupNo")
    public List<NSCStagingStatusInterface> findByGroupNoAndStatus(@Param("groupNo") String groupNo,@Param("status") String status,Pageable pageable);

    @Query("from nscstagingstatus nss,nscstaging ns where ns.id = nss.nscStagingId and nss.status = :status and ns.bplNo = :bplNo")
    public NSCStagingStatusInterface findByStatusAndBPlNo(@Param("status") String status,@Param("bplNo") String bplNo);

    @Query("from nscstagingstatus nss,nscstaging ns where ns.id = nss.nscStagingId and nss.status = :status and ns.employeeNo = :employeeNo")
    public NSCStagingStatusInterface findByStatusAndEmployeeNo(@Param("status") String status,@Param("employeeNo") String employeeNo);

}
