package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.SecurityDepositInterestRate;
import com.mppkvvcl.ngbinterface.interfaces.SecurityDepositInterestRateInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SecurityDepositInterestRateRepository extends JpaRepository<SecurityDepositInterestRate, Long> {

    public SecurityDepositInterestRateInterface save(SecurityDepositInterestRateInterface securityDepositInterestInterface);


    @Query("from SecurityDepositInterestRate sd where (:startDate >= sd.effectiveStartDate and :startDate <= sd.effectiveEndDate) or (:endDate >= sd.effectiveStartDate and :endDate <= sd.effectiveEndDate) order by id asc")
    public List<SecurityDepositInterestRateInterface> findByEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

}
