package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.IrrigationSchemeInterface;
import com.mppkvvcl.ngbentity.beans.IrrigationScheme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IrrigationSchemeRepository extends JpaRepository<IrrigationScheme,Long>{

    public IrrigationSchemeInterface findByConsumerNo(String consumerNo);

    public IrrigationSchemeInterface save(IrrigationSchemeInterface irrigationScheme);
}
