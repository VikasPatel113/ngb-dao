package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerInformationInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PREETESH on 6/7/2017.
 */
@Repository
public interface ConsumerInformationRepository extends JpaRepository<ConsumerInformation,Long> {
    public ConsumerInformationInterface findByConsumerNo(String consumerNo);
    public List<ConsumerInformationInterface> findByConsumerName(String consumerName);

    public ConsumerInformationInterface save(ConsumerInformationInterface consumerInformationInterface);
}
