package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.PurposeSubCategoryMappingInterface;
import com.mppkvvcl.ngbentity.beans.PurposeSubCategoryMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by NITISH on 26-05-2017.
 * This is Repository Interface for purpose_subcategory_mapping table at the backend database.
 * It provides various crud method for operations with the backend table.
 */
@Repository
public interface PurposeSubCategoryMappingRepository extends JpaRepository<PurposeSubCategoryMapping,Long>{

    /**
     * This method fetch PurposeSubCategoryMapping object based on backend table
     * by given parameters below.
     * param purposeOfInstallationId
     * param subcategoryCode
     * return
     */
    public PurposeSubCategoryMappingInterface findByPurposeOfInstallationIdAndSubcategoryCode(long purposeOfInstallationId, long subcategoryCode);

    /**
     * this method gives list of purposesubcategorymapping rows based on passed below parameter
     * param purposeOfInstallationId
     * return
     */
    public List<PurposeSubCategoryMappingInterface> findByPurposeOfInstallationId(long purposeOfInstallationId);

    /**
     * this method gives list of purposesubcategorymapping rows based on passed below parameter
     * param subcategoryCode
     * return
     */
    public List<PurposeSubCategoryMappingInterface> findBySubcategoryCode(long subcategoryCode);

    public PurposeSubCategoryMappingInterface save(PurposeSubCategoryMappingInterface purposeSubCategoryMappingInterface);
}
