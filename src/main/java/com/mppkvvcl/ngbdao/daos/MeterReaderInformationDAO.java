package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.MeterReaderInformationDAOInterface;
import com.mppkvvcl.ngbdao.repositories.MeterReaderInformationRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterReaderInformationInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by ASHISH on 12/14/2017.
 */
@Service
public class MeterReaderInformationDAO implements MeterReaderInformationDAOInterface<MeterReaderInformationInterface> {

    private Logger logger = GlobalResources.getLogger(MeterReaderInformationDAO.class);

    @Autowired
    private MeterReaderInformationRepository meterReaderInformationRepository;

    @Override
    public MeterReaderInformationInterface add(MeterReaderInformationInterface meterReaderInformation) {
        final String methodName = "add()  : ";
        logger.info(methodName + "called");
        MeterReaderInformationInterface insertedMeterRent = null;
        if (meterReaderInformation != null) {
            insertedMeterRent = meterReaderInformationRepository.save(meterReaderInformation);
        }
        return insertedMeterRent;
    }

    @Override
    public MeterReaderInformationInterface update(MeterReaderInformationInterface meterReaderInformation) {
        final String methodName = "update()  : ";
        logger.info(methodName + "called");
        MeterReaderInformationInterface updateMeterRent = null;
        if (meterReaderInformation != null) {
            updateMeterRent = meterReaderInformationRepository.save(meterReaderInformation);
        }
        return updateMeterRent;
    }

    @Override
    public MeterReaderInformationInterface get(MeterReaderInformationInterface meterReaderInformation) {
        final String methodName = "get()  : ";
        logger.info(methodName + "called");
        MeterReaderInformationInterface getMeterRent = null;
        if (meterReaderInformation != null) {
            getMeterRent = meterReaderInformationRepository.findOne(meterReaderInformation.getId());
        }
        return getMeterRent;
    }

    @Override
    public MeterReaderInformationInterface getById(long id) {
        final String methodName = "getById()  : ";
        logger.info(methodName + "called");
        MeterReaderInformationInterface meterReaderInformation = null;
        if (id > 0) {
            meterReaderInformation = meterReaderInformationRepository.findOne(id);
        }
        return meterReaderInformation;
    }

    @Override
    public MeterReaderInformationInterface getByReadingDiaryNoId(long readingDiaryNoId) {
        final String methodName = "getByReadingDiaryNoId()  : ";
        logger.info(methodName + "called");
        MeterReaderInformationInterface meterReaderInformation = null;
        if (readingDiaryNoId > 0l) {
            meterReaderInformation = meterReaderInformationRepository.findByReadingDiaryNoId(readingDiaryNoId);
        }
        return meterReaderInformation;
    }
}
