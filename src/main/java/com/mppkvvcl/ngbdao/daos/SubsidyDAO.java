package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.SubsidyDAOInterface;
import com.mppkvvcl.ngbdao.repositories.SubsidyRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.SubsidyInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class SubsidyDAO implements SubsidyDAOInterface<SubsidyInterface>{

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(SubsidyDAO.class);

    @Autowired
    private SubsidyRepository subsidyRepository;

    @Override
    public List<SubsidyInterface> getByCategoryAndIsBplAndSubcategoryCodeAndDate(String category, boolean isBpl, long subcategoryCode, Date date) {
        final String methodName = "getByCategoryAndIsBplAndSubcategoryCodeAndDate() : ";
        logger.info(methodName + "called");
        List<SubsidyInterface> subsidyInterfaces = null;
        if(category != null && date != null){
            subsidyInterfaces = subsidyRepository.findByCategoryAndIsBplAndSubcategoryCodeAndDate(category,isBpl,subcategoryCode,date);
        }
        return subsidyInterfaces;
    }

    @Override
    public SubsidyInterface add(SubsidyInterface subsidyInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        SubsidyInterface addedSubsidyInterface = null;
        if(subsidyInterface != null){
            addedSubsidyInterface = subsidyRepository.save(subsidyInterface);
        }
        return addedSubsidyInterface;
    }

    @Override
    public SubsidyInterface update(SubsidyInterface subsidyInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        SubsidyInterface updatedSubsidyInterface = null;
        if(subsidyInterface != null){
            updatedSubsidyInterface = subsidyRepository.save(subsidyInterface);
        }
        return updatedSubsidyInterface;
    }

    @Override
    public SubsidyInterface get(SubsidyInterface subsidyInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        SubsidyInterface fetchedSubsidyInterface = null;
        if(subsidyInterface != null){
            fetchedSubsidyInterface = subsidyRepository.findOne(subsidyInterface.getId());
        }
        return fetchedSubsidyInterface;
    }

    @Override
    public SubsidyInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        SubsidyInterface subsidyInterface = null;
        subsidyInterface = subsidyRepository.findOne(id);
        return subsidyInterface;
    }
}
