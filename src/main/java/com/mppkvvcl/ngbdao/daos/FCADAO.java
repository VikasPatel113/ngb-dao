package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.FCADAOInterface;
import com.mppkvvcl.ngbdao.repositories.FCARepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.FCAInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class FCADAO implements FCADAOInterface<FCAInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(FCADAO.class);

    @Autowired
    private FCARepository fcaRepository ;

    @Override
    public FCAInterface add(FCAInterface fca) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        FCAInterface insertedFCA = null;
        if (fca != null){
            insertedFCA = fcaRepository.save(fca);
        }
        return insertedFCA;
    }

    @Override
    public FCAInterface update(FCAInterface fca) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        FCAInterface updatedFCA = null;
        if (fca != null){
            updatedFCA = fcaRepository.save(fca);
        }
        return updatedFCA;
    }

    @Override
    public FCAInterface get(FCAInterface fca) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        FCAInterface existingFCA = null;
        if (fca != null){
            existingFCA = fcaRepository.findOne(fca.getId());
        }
        return existingFCA;
    }

    @Override
    public FCAInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        FCAInterface existingFCA = null;
        if(id != 0){
            existingFCA = fcaRepository.findOne(id);
        }
        return existingFCA;
    }

    @Override
    public FCAInterface getByDate(Date date) {
        final String methodName = "getByDate() : ";
        logger.info(methodName + "called");
        FCAInterface existingFCA = null;
        if(date != null){
            existingFCA = fcaRepository.findByDate(date);
        }
        return existingFCA;
    }

    @Override
    public List<FCAInterface> getByEffectiveStartDateAndEffectiveEndDate(Date startDate, Date endDate) {
        final String methodName = "getByEffectiveStartDateAndEffectiveEndDate() : ";
        logger.info(methodName + "called");
        List<FCAInterface> fcaInterfaces = null;
        if(startDate != null && endDate != null){
            fcaInterfaces = fcaRepository.findByEffectiveStartDateAndEffectiveEndDate(startDate,endDate);
        }
        return fcaInterfaces;
    }
}
