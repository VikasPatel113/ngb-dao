package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.BillDAOInterface;
import com.mppkvvcl.ngbdao.repositories.BillRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.BillInterface;
import com.mppkvvcl.ngbinterface.interfaces.PaymentInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
@Service
public class BillDAO implements BillDAOInterface<BillInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private Logger logger = GlobalResources.getLogger(BillDAO.class);

    /**
     * Requesting spring to get singleton  BillRepository object.
     */
    @Autowired
    private BillRepository billRepository;

    @Override
    public BillInterface getTopByConsumerNoAndDeleted(String consumerNo, boolean deleted) {
        final String methodName = "getTopByConsumerNoAndDeleted() : ";
        logger.info(methodName + "called");
        BillInterface bill = null;
        if(consumerNo != null){
            bill = billRepository.findTopByConsumerNoAndDeletedOrderByIdDesc(consumerNo, deleted);
        }
        return bill;
    }

    @Override
    public BillInterface get(BillInterface bill) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        BillInterface existingBill = null;
        if(bill != null){
            existingBill = billRepository.findOne(bill.getId());
        }
        return existingBill;
    }

    @Override
    public BillInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        BillInterface bill = null;
        bill = billRepository.findOne(id);
        return bill;
    }

    @Override
    public BillInterface add(BillInterface bill) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        BillInterface insertedBill = null;
        if(bill != null){
            insertedBill = billRepository.save(bill);
        }
        return insertedBill;
    }

    @Override
    public BillInterface update(BillInterface bill) {
        final String methodName = "update() : ";
        logger.info(methodName + "is called ");
        BillInterface updatedBill = null;
        if(bill != null){
            updatedBill = billRepository.save(bill);
        }
        return updatedBill;
    }

    @Override
    public BillInterface getTopByConsumerNoAndDeletedOrderByIdDesc(String consumerNo, boolean deletedFalse) {
        final String methodName = "getTopConsumerNoAndDeletedOrderByIdDesc() : ";
        logger.info(methodName + "called");
        BillInterface bill = null;
        if(consumerNo !=null){
            bill = billRepository.findTopByConsumerNoAndDeletedOrderByIdDesc(consumerNo,deletedFalse);
        }
        return bill;
    }

    @Override
    public List<BillInterface> getByConsumerNoAndBillMonthAndDeleted(String consumerNo, String billMonth, boolean deletedFalse) {
        final String methodName = "getByConsumerNoAndBillMonthAndDeleted() : ";
        logger.info(methodName + "called");
        List<BillInterface> bill = null;
        if(consumerNo !=null && billMonth != null){
            bill = billRepository.findByConsumerNoAndBillMonthAndDeleted(consumerNo,billMonth,deletedFalse);
        }
        return bill;
    }

    @Override
    public List<String> getDistinctBillMonthByConsumerNoOrderByIdDesc(String consumerNo, boolean deleted) {
        final String methodName = "getDistinctBillMonthByConsumerNoOrderByIdDesc() : ";
        logger.info(methodName + "called");
        List<String> billMonths = null;
        if(consumerNo !=null){
            billMonths = billRepository.findDistinctBillMonthByConsumerNoOrderByIdDesc(consumerNo,deleted);
        }
        return billMonths;

    }

    @Override
    public List<BillInterface> getAllByConsumerNo(String consumerNo, boolean deleted){
        final String methodName = "getAllByConsumerNo() : ";
        logger.info(methodName + "called");
        List<BillInterface> bills = null;
        if(consumerNo != null){
            bills = billRepository.findByConsumerNoAndDeletedOrderByIdDesc(consumerNo,deleted);
        }
        return bills;
    }

    @Override
    public long getCountByConsumerNo(String consumerNo) {
        final String methodName = "getCountByConsumerNo() : ";
        logger.info(methodName + "called");
        long count = -1;
        if(consumerNo != null){
            count = billRepository.countByConsumerNo(consumerNo);
        }
        return count;
    }

    @Override
    public List<BillInterface> getByConsumerNo(String consumerNo, Pageable pageable) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called with pageable");
        List<BillInterface> billInterfaces = null;
        if(consumerNo != null && pageable != null){
            billInterfaces = billRepository.findByConsumerNo(consumerNo,pageable);
        }
        return billInterfaces;
    }

    @Override
    public List<BillInterface> getByConsumerNoOrderByBillMonthDescendingWithPageable(String consumerNo, Pageable pageable) {
        final String methodName = "getByConsumerNoOrderByBillMonthDescendingWithPageable() : ";
        logger.info(methodName + "called");
        List<BillInterface> billInterfaces = null;
        if(consumerNo != null && pageable != null){
            billInterfaces = billRepository.findByConsumerNoOrderByBillMonthDescendingWithPageable(consumerNo,pageable);
        }
        return billInterfaces;
    }

    @Override
    public List<BillInterface> getByConsumerNoOrderByBillMonthAscendingWithPageable(String consumerNo, Pageable pageable) {
        final String methodName = "getByConsumerNoOrderByBillMonthAscendingWithPageable() : ";
        logger.info(methodName + "called");
        List<BillInterface> billInterfaces = null;
        if(consumerNo != null && pageable != null){
            billInterfaces = billRepository.findByConsumerNoOrderByBillMonthAscendingWithPageable(consumerNo,pageable);
        }
        return billInterfaces;
    }

    @Override
    public List<String> getDistinctBillMonthByLocationCode(String locationCode) {
        final String methodName = "getDistinctBillMonthByLocationCode() : ";
        logger.info(methodName + "called");
        List<String> billMonths = null;
        if (!StringUtils.isEmpty(locationCode)) {
            billMonths = billRepository.findDistinctBillMonthByLocationCode(locationCode);
        }
        return billMonths;
    }

    @Override
    public List<String> getDistinctGroupNoByLocationCodeAndBillMonth(String locationCode, String billMonth) {
        final String methodName = "getDistinctGroupNoByLocationCodeAndBillMonth() : ";
        logger.info(methodName + "called");
        List<String> groupNos = null;
        if (!StringUtils.isEmpty(locationCode) && !StringUtils.isEmpty(billMonth)) {
            groupNos = billRepository.findDistinctGroupNoByLocationCodeAndBillMonth(locationCode, billMonth);
        }
        return groupNos;
    }

    @Override
    public List<BillInterface> getByBillMonthAndGroupNoAndDeleted(String billMonth, String groupNo, boolean deleted) {
        final String methodName = "getByBillMonthAndGroupNoAndDeleted() : ";
        logger.info(methodName + "called");
        List<BillInterface> bills = null;
        if (!StringUtils.isEmpty(billMonth) && !StringUtils.isEmpty(groupNo)) {
            bills = billRepository.findByBillMonthAndGroupNoAndDeleted(billMonth, groupNo, deleted);
        }
        return bills;
    }


    @Override
    public List<BillInterface> getByConsumerNoAndBillMonthOrderByIdAsc(String consumerNo, String billMonth){
        final String methodName = "getByConsumerNoAndBillMonthOrderByIdAsc() : ";
        logger.info(methodName + "called");
        List<BillInterface> billInterfaces = null;
        if(consumerNo != null && billMonth != null){
            billInterfaces = billRepository.findByConsumerNoAndBillMonthOrderByIdAsc(consumerNo,billMonth);
        }
        return billInterfaces;
    }
}
