package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.DiscomDAOInterface;
import com.mppkvvcl.ngbdao.repositories.DiscomRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.DiscomInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
@Service
public class DiscomDAO implements DiscomDAOInterface<DiscomInterface> {

    Logger logger = GlobalResources.getLogger(DiscomDAO.class);

    @Autowired
    DiscomRepository discomRepository;

    @Override
    public DiscomInterface add(DiscomInterface discom) {
        final String methodName  = "add() :  ";
        logger.info(methodName+"clicked");
        DiscomInterface insertedDiscom =  null;
        if (discom != null){
            insertedDiscom = discomRepository.save(discom);
        }
        return insertedDiscom;
    }

    @Override
    public DiscomInterface update(DiscomInterface discom) {
        final String methodName  = "update() :  ";
        logger.info(methodName+"clicked");
        DiscomInterface updatedDiscom =  null;
        if (discom != null){
            updatedDiscom = discomRepository.save(discom);
        }
        return updatedDiscom;
    }

    @Override
    public DiscomInterface get(DiscomInterface discom) {
        final String methodName  = "get() :  ";
        logger.info(methodName+"clicked");
        DiscomInterface getDiscom =  null;
        if (discom != null){
            getDiscom = discomRepository.findOne(discom.getId());
        }
        return getDiscom;
    }

    @Override
    public DiscomInterface getById(long id) {
        final String methodName  = "getById() :  ";
        logger.info(methodName+"clicked");
        DiscomInterface getDiscom =  null;
        if (id > 0){
            getDiscom = discomRepository.findOne(id);
        }
        return getDiscom;
    }

    @Override
    public List<? extends DiscomInterface> getAll() {
        return discomRepository.findAll();
    }
}
