package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ReadingDiaryNoDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ReadingDiaryNoRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ReadingDiaryNoInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by PREETESH on 10/3/2017.
 */
@Service
public class ReadingDiaryNoDAO implements ReadingDiaryNoDAOInterface<ReadingDiaryNoInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(ReadingDiaryNoDAO.class);

    @Autowired
    private ReadingDiaryNoRepository readingDiaryNoRepository;

    @Override
    public ReadingDiaryNoInterface add(ReadingDiaryNoInterface readingDiaryNo) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ReadingDiaryNoInterface insertedReadingDiaryNo = null;
        if (readingDiaryNo != null){
            insertedReadingDiaryNo = readingDiaryNoRepository.save(readingDiaryNo);
        }
        return insertedReadingDiaryNo;
    }

    @Override
    public ReadingDiaryNoInterface update(ReadingDiaryNoInterface readingDiaryNo) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ReadingDiaryNoInterface updatedReadingDiaryNo = null;
        if (readingDiaryNo != null){
            updatedReadingDiaryNo = readingDiaryNoRepository.save(readingDiaryNo);
        }
        return updatedReadingDiaryNo;
    }

    @Override
    public ReadingDiaryNoInterface get(ReadingDiaryNoInterface readingDiaryNoInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ReadingDiaryNoInterface readingDiaryNo = null;
        if (readingDiaryNoInterface != null){
            readingDiaryNo = readingDiaryNoRepository.getOne(readingDiaryNoInterface.getId());
        }
        return readingDiaryNo;
    }

    @Override
    public ReadingDiaryNoInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ReadingDiaryNoInterface readingDiaryNo = null;
        if(id != 0){
            readingDiaryNo = readingDiaryNoRepository.findOne(id);
        }
        return readingDiaryNo;    }


    @Override
    public List<ReadingDiaryNoInterface> getByGroupNo(String groupNo) {
        final String methodName = "getByGroupNoOrderByIdDesc() : ";
        logger.info(methodName + "called");
        List<ReadingDiaryNoInterface> readingDiaryNos = null;
        if(groupNo != null){
            readingDiaryNos = readingDiaryNoRepository.findByGroupNo(groupNo);
        }
        return readingDiaryNos;
    }

    @Override
    public ReadingDiaryNoInterface getByGroupNoAndReadingDiaryNo(String groupNo, String readingDiaryNo) {
        final String methodName = "getByGroupNoAndReadingDiaryNo() : ";
        logger.info(methodName + "called");
        ReadingDiaryNoInterface readingDiaryNoInterface = null;
        if(groupNo != null && readingDiaryNo != null){
            readingDiaryNoInterface = readingDiaryNoRepository.findByGroupNoAndReadingDiaryNo(groupNo,readingDiaryNo);
        }
        return readingDiaryNoInterface;
    }
}
