package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.SECircleMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.SECircleMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.SECircleMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Coded by Nitish on 23-09-2017
 */
@Service
public class SECircleMappingDAO implements SECircleMappingDAOInterface<SECircleMappingInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(SECircleMappingDAO.class);

    @Autowired
    private SECircleMappingRepository seCircleMappingRepository;

    @Override
    public SECircleMappingInterface getByUsername(final String username) {
        final String methodName = "getByUsername() : ";
        logger.info(methodName + "called with username " + username);
        SECircleMappingInterface seCircleMapping = null;
        if(username != null){
            seCircleMapping = seCircleMappingRepository.findByUserDetailUsername(username);
        }
        return seCircleMapping;
    }

    @Override
    public SECircleMappingInterface getByCircleId(long circleId) {
        final String methodName = "getByCircleId() : ";
        logger.info(methodName + "called with circle id " + circleId);
        SECircleMappingInterface seCircleMapping = null;
        seCircleMapping = seCircleMappingRepository.findByCircleId(circleId);
        return seCircleMapping;
    }

    @Override
    public SECircleMappingInterface add(SECircleMappingInterface seCircleMapping) {
        return null;
    }

    @Override
    public SECircleMappingInterface update(SECircleMappingInterface seCircleMapping) {
        return null;
    }

    @Override
    public SECircleMappingInterface get(SECircleMappingInterface seCircleMapping) {
        return null;
    }

    @Override
    public SECircleMappingInterface getById(long id) {
        return null;
    }
}
