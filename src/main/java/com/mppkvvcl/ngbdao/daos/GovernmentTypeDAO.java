package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.GovernmentTypeDAOInterface;
import com.mppkvvcl.ngbdao.repositories.GovernmentTypeRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.GovernmentTypeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class GovernmentTypeDAO implements GovernmentTypeDAOInterface<GovernmentTypeInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(GovernmentTypeDAO.class);

    @Autowired
    private GovernmentTypeRepository governmentTypeRepository;
    
    @Override
    public GovernmentTypeInterface add(GovernmentTypeInterface governmentType) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        GovernmentTypeInterface insertedGovernmentType = null;
        if (governmentType != null){
            insertedGovernmentType = governmentTypeRepository.save(governmentType);
        }
        return insertedGovernmentType;
    }

    @Override
    public GovernmentTypeInterface update(GovernmentTypeInterface governmentType) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        GovernmentTypeInterface updatedGovernmentType = null;
        if (governmentType != null){
            updatedGovernmentType = governmentTypeRepository.save(governmentType);
        }
        return updatedGovernmentType;
    }

    @Override
    public GovernmentTypeInterface get(GovernmentTypeInterface governmentType) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        GovernmentTypeInterface existingGovernmentType = null;
        if (governmentType != null){
            existingGovernmentType = governmentTypeRepository.findOne(governmentType.getId());
        }
        return existingGovernmentType;
    }

    @Override
    public List<? extends GovernmentTypeInterface> getAll() {
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        List<? extends GovernmentTypeInterface> governmentTypes = null;
        governmentTypes = governmentTypeRepository.findAll();

        return governmentTypes;
    }


    @Override
    public GovernmentTypeInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        GovernmentTypeInterface existingGovernmentType = null;
        if(id != 0){
            existingGovernmentType = governmentTypeRepository.findOne(id);
        }
        return existingGovernmentType;
    }

    @Override
    public GovernmentTypeInterface getByType(String type) {
        final String methodName = "getByType() : ";
        logger.info(methodName + "called");
        GovernmentTypeInterface existingGovernmentType = null;
        if(type != null){
            existingGovernmentType = governmentTypeRepository.findByType(type);
        }
        return existingGovernmentType;
    }
}
