package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ConsumerInformationDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ConsumerInformationRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerInformationInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MPWZERP on 15-09-2017.
 */
@Service
public class ConsumerInformationDAO implements ConsumerInformationDAOInterface<ConsumerInformationInterface> {

    Logger logger = GlobalResources.getLogger(ConsumerInformationDAO.class);

    @Autowired
    ConsumerInformationRepository consumerInformationRepository;

    @Override
    public ConsumerInformationInterface add(ConsumerInformationInterface consumerInformation) {
        final String methodName = "add() :  ";
        logger.info(methodName+"clicked");
        ConsumerInformationInterface insertedConsumerInformation = null;
        if (consumerInformation != null){
            insertedConsumerInformation = consumerInformationRepository.save(consumerInformation);
        }
        return insertedConsumerInformation;
    }

    @Override
    public ConsumerInformationInterface update(ConsumerInformationInterface consumerInformation) {
        final String methodName = "update() : ";
        logger.info(methodName+"clicked");
        ConsumerInformationInterface newConsumerInformation =  null;
        if (consumerInformation != null){
            newConsumerInformation = consumerInformationRepository.save(consumerInformation);
        }
        return newConsumerInformation;
    }

    @Override
    public ConsumerInformationInterface get(ConsumerInformationInterface consumerInformation) {
        final String methodName = "get() :  ";
        logger.info(methodName+"clicked");
        ConsumerInformationInterface oldConsumerInformation = null;
        if (consumerInformation != null){
            oldConsumerInformation  = consumerInformationRepository.findOne(consumerInformation.getId());
        }
        return oldConsumerInformation;
    }

    @Override
    public ConsumerInformationInterface getById(long id) {
        final String methoidName = "getById() :  ";
        logger.info(methoidName+"clicked");
        ConsumerInformationInterface consumerInformation = null;
        if (id != 0){
            consumerInformation = consumerInformationRepository.findOne(id);
        }
        return consumerInformation;
    }

    @Override
    public ConsumerInformationInterface getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() :  ";
        logger.info(methodName+"clicked");
        ConsumerInformationInterface consumerInformation = null;
        if (consumerNo != null){
            consumerInformation = consumerInformationRepository.findByConsumerNo(consumerNo);
        }
        return consumerInformation;
    }

    @Override
    public List<ConsumerInformationInterface> getByConsumerName(String consumerName) {
        final String methodName = "getByConsumerName()  : ";
        logger.info(methodName+"clicked");
        List<ConsumerInformationInterface> consumerInformationList = null;
        if (consumerName != null){
            consumerInformationList = consumerInformationRepository.findByConsumerName(consumerName);
        }
        return consumerInformationList;
    }
}
