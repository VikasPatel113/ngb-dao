package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.TariffLoadMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.TariffLoadMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.TariffLoadMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TariffLoadMappingDAO implements TariffLoadMappingDAOInterface<TariffLoadMappingInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private Logger logger = GlobalResources.getLogger(TariffLoadMappingDAO.class);

    @Autowired
    private TariffLoadMappingRepository tariffLoadMappingRepository;

    @Override
    public List<TariffLoadMappingInterface> getByTariffDetailId(long tariffId) {
        final String methodName = "getByTariffDetailId() : ";
        logger.info(methodName + "called");
        List<TariffLoadMappingInterface> tariffLoadMappingInterfaces = null;
        tariffLoadMappingInterfaces = tariffLoadMappingRepository.findByTariffDetailId(tariffId);
        return tariffLoadMappingInterfaces;
    }

    @Override
    public List<TariffLoadMappingInterface> getByLoadDetailId(long loadId) {
        final String methodName = "getByLoadDetailId() : ";
        logger.info(methodName + "called");
        List<TariffLoadMappingInterface> tariffLoadMappingInterfaces = null;
        tariffLoadMappingInterfaces = tariffLoadMappingRepository.findByLoadDetailId(loadId);
        return tariffLoadMappingInterfaces;
    }

    @Override
    public List<TariffLoadMappingInterface> getByTariffDetailIdAndLoadDetailId(long tariffId, long loadId) {
        final String methodName = "getByTariffDetailIdAndLoadDetailId() : ";
        logger.info(methodName + "called");
        List<TariffLoadMappingInterface> tariffLoadMappingInterfaces = null;
        tariffLoadMappingInterfaces = tariffLoadMappingRepository.findByTariffDetailIdAndLoadDetailId(tariffId,loadId);
        return tariffLoadMappingInterfaces;
    }

    @Override
    public List<TariffLoadMappingInterface> getByTariffDetailIdAndLoadDetailIdAndIsTariffChangeAndIsLoadChange(long tariffId, long loadId, boolean isTariffChange, boolean isLoadChange) {
        final String methodName = "getByTariffDetailIdAndLoadDetailIdAndIsTariffChangeAndIsLoadChange() : ";
        logger.info(methodName + "called");
        List<TariffLoadMappingInterface> tariffLoadMappingInterfaces = null;
        tariffLoadMappingInterfaces = tariffLoadMappingRepository.findByTariffDetailIdAndLoadDetailIdAndIsTariffChangeAndIsLoadChange(tariffId,loadId,isTariffChange,isLoadChange);
        return tariffLoadMappingInterfaces;
    }

    @Override
    public List<TariffLoadMappingInterface> getByTariffDetailIdAndLoadDetailIdAndIsTariffChange(long tariffId, long loadId, boolean isTariffChange) {
        final String methodName = "getByTariffDetailIdAndLoadDetailIdAndIsTariffChange() : ";
        logger.info(methodName + "called");
        List<TariffLoadMappingInterface> tariffLoadMappingInterfaces = null;
        tariffLoadMappingInterfaces = tariffLoadMappingRepository.findByTariffDetailIdAndLoadDetailIdAndIsTariffChange(tariffId,loadId,isTariffChange);
        return tariffLoadMappingInterfaces;
    }

    @Override
    public TariffLoadMappingInterface getTopByTariffDetailIdOrderByIdDesc(long tariffId) {
        final String methodName = "getTopByTariffDetailIdOrderByIdDesc() : ";
        logger.info(methodName + "called");
        TariffLoadMappingInterface tariffLoadMappingInterface = null;
        tariffLoadMappingInterface = tariffLoadMappingRepository.findTopByTariffDetailIdOrderByIdDesc(tariffId);
        return tariffLoadMappingInterface;
    }

    @Override
    public TariffLoadMappingInterface getTopByLoadDetailIdOrderByIdDesc(long loadId) {
        final String methodName = "getTopByLoadDetailIdOrderByIdDesc() : ";
        logger.info(methodName + "called");
        TariffLoadMappingInterface tariffLoadMappingInterface = null;
        tariffLoadMappingInterface = tariffLoadMappingRepository.findTopByLoadDetailIdOrderByIdDesc(loadId);
        return tariffLoadMappingInterface;
    }


    @Override
    public TariffLoadMappingInterface add(TariffLoadMappingInterface tariffLoadMappingInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        TariffLoadMappingInterface addedTariffLoadMappingInterface = null;
        if(tariffLoadMappingInterface != null){
            addedTariffLoadMappingInterface = tariffLoadMappingRepository.save(tariffLoadMappingInterface);
        }
        return addedTariffLoadMappingInterface;
    }

    @Override
    public TariffLoadMappingInterface update(TariffLoadMappingInterface tariffLoadMappingInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        TariffLoadMappingInterface updatedTariffLoadMappingInterface = null;
        if(tariffLoadMappingInterface != null){
            updatedTariffLoadMappingInterface = tariffLoadMappingRepository.save(tariffLoadMappingInterface);
        }
        return updatedTariffLoadMappingInterface;
    }

    @Override
    public TariffLoadMappingInterface get(TariffLoadMappingInterface tariffLoadMappingInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        TariffLoadMappingInterface fetchedTariffLoadMappingInterface = null;
        if(tariffLoadMappingInterface != null){
            fetchedTariffLoadMappingInterface = tariffLoadMappingRepository.findOne(tariffLoadMappingInterface.getId());
        }
        return fetchedTariffLoadMappingInterface;
    }

    @Override
    public TariffLoadMappingInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        TariffLoadMappingInterface tariffLoadMappingInterface = null;
        tariffLoadMappingInterface = tariffLoadMappingRepository.findOne(id);
        return tariffLoadMappingInterface;
    }

}
