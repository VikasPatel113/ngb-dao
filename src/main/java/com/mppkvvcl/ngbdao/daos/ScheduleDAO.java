package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ScheduleDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ScheduleRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ScheduleInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class ScheduleDAO implements ScheduleDAOInterface<ScheduleInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(ScheduleDAO.class);

    @Autowired
    ScheduleRepository scheduleRepository = null;
    
    @Override
    public ScheduleInterface add(ScheduleInterface schedule) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ScheduleInterface insertedSchedule = null;
        if (schedule != null){
            insertedSchedule = scheduleRepository.save(schedule);
        }
        return insertedSchedule;
    }

    @Override
    public ScheduleInterface update(ScheduleInterface schedule) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ScheduleInterface updatedSchedule = null;
        if (schedule != null){
            updatedSchedule = scheduleRepository.save(schedule);
        }
        return updatedSchedule;
    }

    @Override
    public ScheduleInterface get(ScheduleInterface schedule) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ScheduleInterface existingSchedule = null;
        if (schedule != null){
            existingSchedule = scheduleRepository.findOne(schedule.getId());
        }
        return existingSchedule;
    }

    @Override
    public ScheduleInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ScheduleInterface existingSchedule = null;
        if(id != 0){
            existingSchedule = scheduleRepository.findOne(id);
        }
        return existingSchedule;    }

    @Override
    public List<ScheduleInterface> getByGroupNoOrderByIdDesc(String groupNo) {
        final String methodName = "getByGroupNoOrderByIdDesc() : ";
        logger.info(methodName + "called");
        List<ScheduleInterface> fetchedSchedule = null;
        if(groupNo != null){
            fetchedSchedule = scheduleRepository.findByGroupNoOrderByIdDesc(groupNo);
        }
        return fetchedSchedule;
    }

    @Override
    public List<ScheduleInterface> getByGroupNo(String groupNo, Pageable pageable) {
        final String methodName = "getByGroupNo() : ";
        logger.info(methodName + "called with pageable");
        List<ScheduleInterface> scheduleInterfaces = null;
        if(groupNo != null && pageable != null){
            scheduleInterfaces = scheduleRepository.findByGroupNo(groupNo,pageable);
        }
        return scheduleInterfaces;
    }

    @Override
    public List<ScheduleInterface> getByGroupNoOrderByBillMonthDescending(String groupNo, Pageable pageable) {
        final String methodName = "getByGroupNoOrderByBillMonthDescending() : ";
        logger.info(methodName + "called with pageable");
        List<ScheduleInterface> scheduleInterfaces = null;
        if(groupNo != null && pageable != null){
            scheduleInterfaces = scheduleRepository.findByGroupNoOrderByBillMonthDescending(groupNo,pageable);
        }
        return scheduleInterfaces;
    }

    @Override
    public List<ScheduleInterface> getByGroupNoOrderByBillMonthAscending(String groupNo, Pageable pageable) {
        final String methodName = "getByGroupNoOrderByBillMonthAscending() : ";
        logger.info(methodName + "called with pageable");
        List<ScheduleInterface> scheduleInterfaces = null;
        if(groupNo != null && pageable != null){
            scheduleInterfaces = scheduleRepository.findByGroupNoOrderByBillMonthAscending(groupNo,pageable);
        }
        return scheduleInterfaces;
    }

    @Override
    public List<ScheduleInterface> getByBillMonth(String billMonth) {
        final String methodName = "getByBillMonth() : ";
        logger.info(methodName + "called");
        List<ScheduleInterface> fetchedSchedule = null;
        if(billMonth != null){
            fetchedSchedule = scheduleRepository.findByBillMonth(billMonth);
        }
        return fetchedSchedule;
    }

    @Override
    public List<ScheduleInterface> getByBillStatus(String billStatus) {
        final String methodName = "getByBillStatus() : ";
        logger.info(methodName + "called");
        List<ScheduleInterface> fetchedSchedule = null;
        if(billStatus != null){
            fetchedSchedule = scheduleRepository.findByBillStatus(billStatus);
        }
        return fetchedSchedule;
    }

    @Override
    public List<ScheduleInterface> getByBillMonthAndBillStatus(String billMonth, String billStatus) {
        final String methodName = "getByBillMonthAndBillStatus() : ";
        logger.info(methodName + "called");
        List<ScheduleInterface> fetchedSchedule = null;
        if(billMonth != null && billStatus != null){
            fetchedSchedule = scheduleRepository.findByBillMonthAndBillStatus(billMonth,billStatus);
        }
        return fetchedSchedule;
    }

    @Override
    public List<ScheduleInterface> getByBillStatusAndSubmitted(String billStatus, String submitted) {
        final String methodName = "getByBillStatusAndSubmitted() : ";
        logger.info(methodName + "called");
        List<ScheduleInterface> fetchedSchedule = null;
        if(submitted != null && billStatus != null){
            fetchedSchedule = scheduleRepository.findByBillStatusAndSubmitted(billStatus,submitted);
        }
        return fetchedSchedule;
    }

    @Override
    public List<ScheduleInterface> getByR15Status(String r15Status) {
        final String methodName = "getByR15Status() : ";
        logger.info(methodName + "called");
        List<ScheduleInterface> fetchedSchedule = null;
        if(r15Status != null){
            fetchedSchedule = scheduleRepository.findByR15Status(r15Status);
        }
        return fetchedSchedule;
    }

    @Override
    public ScheduleInterface getByGroupNoAndBillMonthAndBillStatus(String groupNo, String billMonth, String billStatus) {
        final String methodName = "getByGroupNoAndBillMonthAndBillStatus() : ";
        logger.info(methodName + "called");
        ScheduleInterface fetchedSchedule = null;
        if(groupNo != null && billMonth != null && billStatus != null){
            fetchedSchedule = scheduleRepository.findByGroupNoAndBillMonthAndBillStatus(groupNo , billMonth , billStatus);
        }
        return fetchedSchedule;
    }

    @Override
    public List<ScheduleInterface> getByBillMonthAndBillStatusAndSubmitted(String billMonth, String billStatus, String submitted) {
        final String methodName = "getByBillMonthAndBillStatusAndSubmitted() : ";
        logger.info(methodName + "called");
        List<ScheduleInterface> fetchedSchedule = null;
        if(billMonth != null && billStatus != null && submitted != null){
            fetchedSchedule = scheduleRepository.findByBillMonthAndBillStatusAndSubmitted(billMonth , billStatus , submitted);
        }
        return fetchedSchedule;
    }

    @Override
    public List<ScheduleInterface> getByBillMonthAndR15Status(String billMonth, String r15Status) {
        final String methodName = "getByBillMonthAndR15Status() : ";
        logger.info(methodName + "called");
        List<ScheduleInterface> fetchedSchedule = null;
        if(billMonth != null && r15Status != null){
            fetchedSchedule = scheduleRepository.findByBillMonthAndR15Status(billMonth,r15Status);
        }
        return fetchedSchedule;
    }

    @Override
    public List<ScheduleInterface> getByGroupNoAndBillStatusAndSubmitted(String groupNo, String billStatus, String submitted) {
        final String methodName = "getByGroupNoAndBillStatusAndSubmitted() : ";
        logger.info(methodName + "called");
        List<ScheduleInterface> fetchedSchedule = null;
        if(groupNo != null && billStatus != null && submitted != null){
            fetchedSchedule = scheduleRepository.findByGroupNoAndBillStatusAndSubmitted(groupNo , billStatus , submitted);
        }
        return fetchedSchedule;
    }

    @Override
    public ScheduleInterface getTopByGroupNoOrderByIdDesc(String groupNo) {
        final String methodName = "getTopByGroupNoOrderByIdDesc() : ";
        logger.info(methodName + "called");
        ScheduleInterface fetchedSchedule = null;
        if(groupNo != null){
            fetchedSchedule = scheduleRepository.findTopByGroupNoOrderByIdDesc(groupNo);
        }
        return fetchedSchedule;    
    }

    @Override
    public ScheduleInterface getTopByGroupNoAndBillStatusOrderByIdDesc(String groupNo, String billStatus) {
        final String methodName = "getTopByGroupNoAndBillStatusOrderByIdDesc() : ";
        logger.info(methodName + "called");
        ScheduleInterface fetchedSchedule = null;
        if(groupNo != null && billStatus != null){
            fetchedSchedule = scheduleRepository.findTopByGroupNoAndBillStatusOrderByIdDesc(groupNo,billStatus);
        }
        return fetchedSchedule;
    }

    @Override
    public ScheduleInterface getByGroupNoAndBillMonth(String groupNo, String lastBillMonth) {
        final String methodName = "getByGroupNoAndBillMonth() : ";
        logger.info(methodName + "called");
        ScheduleInterface fetchedSchedule = null;
        if(groupNo != null && lastBillMonth != null){
            fetchedSchedule = scheduleRepository.findByGroupNoAndBillMonth(groupNo,lastBillMonth);
        }
        return fetchedSchedule;
    }

    @Override
    public long getCountByGroupNo(String groupNo) {
        final String methodName = "getCountByGroupNo() : ";
        logger.info(methodName + "called");
        long fetchedSchedule = 0l;
        if(groupNo != null){
            fetchedSchedule = scheduleRepository.countByGroupNo(groupNo);
        }
        return fetchedSchedule;
    }

    @Override
    public List<ScheduleInterface> getTop2ByGroupNoOrderByIdDesc(String groupNo) {
        final String methodName = "getTop2ByGroupNoOrderByIdDesc() : ";
        logger.info(methodName + "called for groupNo " + groupNo);
        List<ScheduleInterface> schedules = null;
        if(groupNo != null){
            schedules = scheduleRepository.findTop2ByGroupNoOrderByIdDesc(groupNo);
        }
        return schedules;
    }

    @Override
    public ScheduleInterface getByGroupNoAndBillMonthAndBillStatusAndSubmitted(String groupNo, String previousBillMonth, String billStatus, String submittedStatus) {
        final String methodName = "getByGroupNoAndBillMonthAndBillStatusAndSubmitted() : ";
        logger.info(methodName + "called");
        ScheduleInterface scheduleInterface = null;
        if(groupNo != null && previousBillMonth != null && billStatus != null && submittedStatus != null){
            scheduleInterface = scheduleRepository.findByGroupNoAndBillMonthAndBillStatusAndSubmitted(groupNo,previousBillMonth,billStatus,submittedStatus);
        }
        return scheduleInterface;
    }
}
