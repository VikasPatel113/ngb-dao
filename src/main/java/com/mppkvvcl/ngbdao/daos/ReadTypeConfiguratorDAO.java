package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ReadTypeConfiguratorDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ReadTypeConfiguratorRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ReadTypeConfiguratorInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class ReadTypeConfiguratorDAO implements ReadTypeConfiguratorDAOInterface<ReadTypeConfiguratorInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(ReadTypeConfiguratorDAO.class);

    @Autowired
    ReadTypeConfiguratorRepository readTypeConfiguratorRepository = null;


    @Override
    public ReadTypeConfiguratorInterface add(ReadTypeConfiguratorInterface readTypeConfigurator) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ReadTypeConfiguratorInterface insertedReadTypeConfigurator = null;
        if (readTypeConfigurator != null){
            insertedReadTypeConfigurator = readTypeConfiguratorRepository.save(readTypeConfigurator);
        }
        return insertedReadTypeConfigurator;    
    }

    @Override
    public ReadTypeConfiguratorInterface update(ReadTypeConfiguratorInterface readTypeConfigurator) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ReadTypeConfiguratorInterface updatedReadTypeConfigurator = null;
        if (readTypeConfigurator != null){
            updatedReadTypeConfigurator = readTypeConfiguratorRepository.save(readTypeConfigurator);
        }
        return updatedReadTypeConfigurator;    }

    @Override
    public ReadTypeConfiguratorInterface get(ReadTypeConfiguratorInterface readTypeConfigurator) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ReadTypeConfiguratorInterface existingReadTypeConfigurator = null;
        if (readTypeConfigurator != null){
            existingReadTypeConfigurator = readTypeConfiguratorRepository.findOne(readTypeConfigurator.getId());
        }
        return existingReadTypeConfigurator;    }

    @Override
    public ReadTypeConfiguratorInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ReadTypeConfiguratorInterface existingReadTypeConfigurator = null;
        if(id != 0){
            existingReadTypeConfigurator = readTypeConfiguratorRepository.findOne(id);
        }
        return existingReadTypeConfigurator;
    }


    @Override
    public ReadTypeConfiguratorInterface getByTariffIdAndSubcategoryCode(Long tariffId, Long subcategoryCode) {
        final String methodName = "getByTariffIdAndSubcategoryCode() : ";
        logger.info(methodName + "called");
        ReadTypeConfiguratorInterface fetchedReadTypeConfigurator = null;
        if(tariffId != 0 && subcategoryCode != 0){
            fetchedReadTypeConfigurator = readTypeConfiguratorRepository.findByTariffIdAndSubcategoryCode(tariffId , subcategoryCode);
        }
        return fetchedReadTypeConfigurator;
    }

    @Override
    public List<? extends ReadTypeConfiguratorInterface> getAll() {
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        return readTypeConfiguratorRepository.findAll();
    }
}
