package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.OICZoneMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.OICZoneMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.OICZoneMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * code by nitish on 23-07-2017
 */
@Service
public class OICZoneMappingDAO implements OICZoneMappingDAOInterface<OICZoneMappingInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(OICZoneMappingDAO.class);

    @Autowired
    private OICZoneMappingRepository oicZoneMappingRepository;

    @Override
    public OICZoneMappingInterface getByUsername(final String username) {
        final String methodName = "getByUsername() : ";
        logger.info(methodName + "called with username " + username);
        OICZoneMappingInterface oicZoneMapping = null;
        if(username != null){
            oicZoneMapping = oicZoneMappingRepository.findByUserDetailUsername(username);
        }
        return oicZoneMapping;
    }

    @Override
    public OICZoneMappingInterface getByZoneId(final long zoneId) {
        final String methodName = "getByZoneId() : ";
        logger.info(methodName + "called with zone id " + zoneId);
        OICZoneMappingInterface oicZoneMapping = null;
        oicZoneMapping = oicZoneMappingRepository.findByZoneId(zoneId);
        return oicZoneMapping;
    }

    @Override
    public OICZoneMappingInterface add(OICZoneMappingInterface oicZoneMapping) {
        return null;
    }

    @Override
    public OICZoneMappingInterface update(OICZoneMappingInterface oicZoneMapping) {
        return null;
    }

    @Override
    public OICZoneMappingInterface get(OICZoneMappingInterface oicZoneMapping) {
        return null;
    }

    @Override
    public OICZoneMappingInterface getById(long id) {
        return null;
    }
}
