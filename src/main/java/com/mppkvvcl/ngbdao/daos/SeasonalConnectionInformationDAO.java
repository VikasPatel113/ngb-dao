package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.SeasonalConnectionInformationDAOInterface;
import com.mppkvvcl.ngbdao.repositories.SeasonalConnectionInformationRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.SeasonalConnectionInformationInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class SeasonalConnectionInformationDAO implements SeasonalConnectionInformationDAOInterface<SeasonalConnectionInformationInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(SeasonalConnectionInformationDAO.class);

    @Autowired
    SeasonalConnectionInformationRepository seasonalConnectionInformationRepository = null;



    @Override
    public SeasonalConnectionInformationInterface add(SeasonalConnectionInformationInterface seasonalConnectionInformation) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        SeasonalConnectionInformationInterface insertedSeasonalConnectionInformation = null;
        if (seasonalConnectionInformation != null){
            insertedSeasonalConnectionInformation = seasonalConnectionInformationRepository.save(seasonalConnectionInformation);
        }
        return insertedSeasonalConnectionInformation;    }

    @Override
    public SeasonalConnectionInformationInterface update(SeasonalConnectionInformationInterface seasonalConnectionInformation) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        SeasonalConnectionInformationInterface updatedSeasonalConnectionInformation = null;
        if (seasonalConnectionInformation != null){
            updatedSeasonalConnectionInformation = seasonalConnectionInformationRepository.save(seasonalConnectionInformation);
        }
        return updatedSeasonalConnectionInformation;
    }

    @Override
    public SeasonalConnectionInformationInterface get(SeasonalConnectionInformationInterface seasonalConnectionInformation) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        SeasonalConnectionInformationInterface existingSeasonalConnectionInformation = null;
        if (seasonalConnectionInformation != null){
            existingSeasonalConnectionInformation = seasonalConnectionInformationRepository.findOne(seasonalConnectionInformation.getId());
        }
        return existingSeasonalConnectionInformation;
    }

    @Override
    public SeasonalConnectionInformationInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        SeasonalConnectionInformationInterface existingSeasonalConnectionInformation = null;
        if(id != 0){
            existingSeasonalConnectionInformation = seasonalConnectionInformationRepository.findOne(id);
        }
        return existingSeasonalConnectionInformation;
    }

    @Override
    public List<SeasonalConnectionInformationInterface> getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        List<SeasonalConnectionInformationInterface> fetchedSeasonalConnectionInformation = null;
        if(consumerNo != null){
            fetchedSeasonalConnectionInformation = seasonalConnectionInformationRepository.findByConsumerNo(consumerNo);
        }
        return fetchedSeasonalConnectionInformation;
    }

    @Override
    public List<SeasonalConnectionInformationInterface> getBySeasonStartDateAndSeasonEndDate(Date givenDate) {
        final String methodName = "getBySeasonStartDateAndSeasonEndDate() : ";
        logger.info(methodName + "called");
        List<SeasonalConnectionInformationInterface> fetchedSeasonalConnectionInformation = null;
        if(givenDate != null ){
            fetchedSeasonalConnectionInformation = seasonalConnectionInformationRepository.findBySeasonStartDateAndSeasonEndDate(givenDate);
        }
        return fetchedSeasonalConnectionInformation;
    }

    @Override
    public List<SeasonalConnectionInformationInterface> getBetweenSeasonStartDateAndSeasonEndDateAndConsumerNo(Date givenDate, String consumerNo) {
        final String methodName = "getBetweenSeasonStartDateAndSeasonEndDateAndConsumerNo() : ";
        logger.info(methodName + "called");
        List<SeasonalConnectionInformationInterface> fetchedSeasonalConnectionInformation = null;
        if(givenDate != null && consumerNo != null){
            fetchedSeasonalConnectionInformation = seasonalConnectionInformationRepository.findBetweenSeasonStartDateAndSeasonEndDateAndConsumerNo(givenDate , consumerNo);
        }
        return fetchedSeasonalConnectionInformation;
    }

    @Override
    public List<SeasonalConnectionInformationInterface> getByViolationMonth(String violationMonth) {
        final String methodName = "getByViolationMonth() : ";
        logger.info(methodName + "called");
        List<SeasonalConnectionInformationInterface> fetchedSeasonalConnectionInformation = null;
        if(violationMonth != null ){
            fetchedSeasonalConnectionInformation = seasonalConnectionInformationRepository.findByViolationMonth(violationMonth);
        }
        return fetchedSeasonalConnectionInformation;
    }

    @Override
    public SeasonalConnectionInformationInterface getTopByConsumerNoOrderByIdDesc(String consumerNo) {
        final String methodName  = "getTopByConsumerNoOrderByIdDesc()  : ";
        logger.info(methodName+"called");
        SeasonalConnectionInformationInterface seasonalConnectionInformation = null;
        if (consumerNo != null){
            seasonalConnectionInformation = seasonalConnectionInformationRepository.findTopByConsumerNoOrderByIdDesc(consumerNo);
        }
        return seasonalConnectionInformation;
    }
}
