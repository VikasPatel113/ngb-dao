package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ConsumerMeterMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ConsumerMeterMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerMeterMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
@Service
public class ConsumerMeterMappingDAO implements ConsumerMeterMappingDAOInterface<ConsumerMeterMappingInterface> {

    private Logger logger = GlobalResources.getLogger(ConsumerMeterMappingDAO.class);

    @Autowired
    private ConsumerMeterMappingRepository consumerMeterMappingRepository;

    @Override
    public ConsumerMeterMappingInterface add(ConsumerMeterMappingInterface consumerMeterMapping) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ConsumerMeterMappingInterface insertedConsumerMeterMapping = null;
        if (consumerMeterMapping != null){
            insertedConsumerMeterMapping = consumerMeterMappingRepository.save(consumerMeterMapping);
        }
        return insertedConsumerMeterMapping;
    }

    @Override
    public ConsumerMeterMappingInterface update(ConsumerMeterMappingInterface consumerMeterMapping) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ConsumerMeterMappingInterface updatedConsumerMeterMapping = null;
        if (consumerMeterMapping != null){
            updatedConsumerMeterMapping = consumerMeterMappingRepository.save(consumerMeterMapping);
        }
        return updatedConsumerMeterMapping;
    }

    @Override
    public ConsumerMeterMappingInterface get(ConsumerMeterMappingInterface consumerMeterMapping) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ConsumerMeterMappingInterface getConsumerMeterMapping = null;
        if (consumerMeterMapping != null){
            getConsumerMeterMapping = consumerMeterMappingRepository.findOne(consumerMeterMapping.getId());
        }
        return getConsumerMeterMapping;
    }

    @Override
    public ConsumerMeterMappingInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ConsumerMeterMappingInterface consumerMeterMapping = null;
        if (id > 0){
            consumerMeterMapping = consumerMeterMappingRepository.findOne(id);
        }
        return consumerMeterMapping;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() :  ";
        logger.info(methodName + "called");
        List<ConsumerMeterMappingInterface> consumerMeterMappings = null;
        if (consumerNo != null){
            consumerMeterMappings = consumerMeterMappingRepository.findByConsumerNo(consumerNo);
        }
        return consumerMeterMappings;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByMeterIdentifier(String meterIdentifier) {
        final String methodName = "getByMeterIdentifier() : ";
        logger.info(methodName + "called");
        List<ConsumerMeterMappingInterface> consumerMeterMappings = null;
        if (meterIdentifier != null){
            consumerMeterMappings = consumerMeterMappingRepository.findByMeterIdentifier(meterIdentifier);
        }
        return consumerMeterMappings;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByMeterIdentifierAndMappingStatus(String meterIdentifier, String mappingStatus) {
        final String methodName = "getByMeterIdentifierAndMappingStatus() :  ";
        logger.info(methodName + "called");
        List<ConsumerMeterMappingInterface> consumerMeterMappings = null;
        if (meterIdentifier != null && mappingStatus != null){
            consumerMeterMappings = consumerMeterMappingRepository.findByMeterIdentifierAndMappingStatus(meterIdentifier , mappingStatus);
        }
        return consumerMeterMappings;
    }

    @Override
    public ConsumerMeterMappingInterface getByConsumerNoAndMeterIdentifier(String consumerNo, String meterIdentifier) {
        final String methodName = "getByConsumerNoAndMeterIdentifier()  :  ";
        logger.info(methodName + "called");
        ConsumerMeterMappingInterface consumerMeterMapping =  null;
        if (consumerNo != null && meterIdentifier != null){
            consumerMeterMapping = consumerMeterMappingRepository.findByConsumerNoAndMeterIdentifier(consumerNo , meterIdentifier);
        }
        return consumerMeterMapping;
    }

    @Override
    public ConsumerMeterMappingInterface getByConsumerNoAndMeterIdentifierAndMappingStatus(String consumerNo, String meterIdentifier, String mappingStatus) {
        final  String methodName = "getByConsumerNoAndMeterIdentifierAndMappingStatus() :  ";
        logger.info(methodName + "called");
        ConsumerMeterMappingInterface consumerMeterMapping =  null;
        if (consumerNo != null && meterIdentifier != null && mappingStatus != null){
            consumerMeterMapping  = consumerMeterMappingRepository.findByConsumerNoAndMeterIdentifierAndMappingStatus(consumerNo , meterIdentifier , mappingStatus);
        }
        return consumerMeterMapping;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByMeterSerialNo(String meterSerialNo) {
        final String methodName  = "getByMeterSerialNo()  : ";
        logger.info(methodName + "called");
        List<ConsumerMeterMappingInterface> consumerMeterMappings =  null;
        if (meterSerialNo != null){
            consumerMeterMappings = consumerMeterMappingRepository.findByMeterSerialNo(meterSerialNo);
        }
        return consumerMeterMappings;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByMeterMake(String meterMake) {
        final String methodName = "getByMeterMake() : ";
        logger.info(methodName + "called");
        List<ConsumerMeterMappingInterface> consumerMeterMappings =  null;
        if (meterMake != null){
            consumerMeterMappings = consumerMeterMappingRepository.findByMeterMake(meterMake);
        }
        return consumerMeterMappings;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByConsumerNoAndMappingStatus(String consumerNo, String mappingStatus) {
        final String methodName = "getByConsumerNoAndMappingStatus() :  ";
        logger.info(methodName + "called " + consumerNo + " " + mappingStatus);
        List<ConsumerMeterMappingInterface> consumerMeterMappings =  null;
        if (consumerNo != null && mappingStatus != null){
            consumerMeterMappings = consumerMeterMappingRepository.findByConsumerNoAndMappingStatus(consumerNo , mappingStatus);
        }
        return consumerMeterMappings;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByInstallationDateBetween(Date startDate, Date endDate) {
        final String methodName = "getByInstallationDateBetween() :  ";
        logger.info(methodName + "called");
        List<ConsumerMeterMappingInterface> consumerMeterMappings = null;
        if (startDate != null && endDate != null){
            consumerMeterMappings = consumerMeterMappingRepository.findByInstallationDateBetween(startDate , endDate);
        }
        return consumerMeterMappings;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByRemovalDateBetween(Date startDate, Date endDate) {
        final String methodName  = "getByRemovalDateBetween() : ";
        logger.info(methodName + "called");
        List<ConsumerMeterMappingInterface> consumerMeterMappings = null;
        if (startDate != null && endDate != null ){
            consumerMeterMappings = consumerMeterMappingRepository.findByRemovalDateBetween(startDate , endDate);
        }
        return consumerMeterMappings;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByInstallationBillMonth(String installationBillMonthStart, String installationBillMonthEnd) {
        final String methodName = "getByInstallationBillMonth() :  ";
        logger.info(methodName + "called");
        List<ConsumerMeterMappingInterface> consumerMeterMappings = null;
        if (installationBillMonthStart != null && installationBillMonthEnd != null){
            consumerMeterMappings = consumerMeterMappingRepository.findByInstallationBillMonth(installationBillMonthStart , installationBillMonthEnd);
        }
        return consumerMeterMappings;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByConsumerNoAndInstallationBillMonth(String consumerNo, String installationBillMonthStart, String installationBillMonthEnd) {
        final String methodName = "getByConsumerNoAndInstallationBillMonth() : ";
        logger.info(methodName + "called");
        List<ConsumerMeterMappingInterface> consumerMeterMappings = null;
        if (consumerNo != null && installationBillMonthStart != null && installationBillMonthEnd != null){
            consumerMeterMappings = consumerMeterMappingRepository.findByConsumerNoAndInstallationBillMonth(consumerNo , installationBillMonthStart , installationBillMonthEnd);
        }
        return consumerMeterMappings;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByRemovalBillMonth(String removalBillMonthStart, String removalBillMonthEnd) {
        final String methodName = "getByRemovalBillMonth() :  ";
        logger.info(methodName + "called");
        List<ConsumerMeterMappingInterface> consumerMeterMappings =  null;
        if (removalBillMonthStart != null && removalBillMonthEnd != null){
            consumerMeterMappings = consumerMeterMappingRepository.findByRemovalBillMonth(removalBillMonthStart, removalBillMonthEnd);
        }
        return consumerMeterMappings;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByConsumerNoAndRemovalBillMonth(String consumerNo, String removalBillMonthStart, String removalBillMonthEnd) {
        final String methodName = "getByConsumerNoAndRemovalBillMonth() : ";
        logger.info(methodName + "called");
        List<ConsumerMeterMappingInterface> consumerMeterMappings = null;
        if (consumerNo != null && removalBillMonthStart != null && removalBillMonthEnd != null){
            consumerMeterMappings =  consumerMeterMappingRepository.findByConsumerNoAndRemovalBillMonth(consumerNo , removalBillMonthStart , removalBillMonthEnd);
        }
        return consumerMeterMappings;
    }

    //count methods

    @Override
    public long getCountByConsumerNo(String consumerNo) {
        final String methodName = "getCountByConsumerNo() : ";
        logger.info(methodName + "called");
        long count = -1;
        if(consumerNo != null){
            count = consumerMeterMappingRepository.countByConsumerNo(consumerNo);
        }
        return count;
    }

    @Override
    public long getCountByMeterIdentifier(String meterIdentifier) {
        final String methodName = "getCountByMeterIdentifier() : ";
        logger.info(methodName + "called");
        long count = -1;
        if(meterIdentifier != null){
            count = consumerMeterMappingRepository.countByMeterIdentifier(meterIdentifier);
        }
        return count;
    }

    //pageable methods
    @Override
    public List<ConsumerMeterMappingInterface> getByConsumerNo(String consumerNo, Pageable pageable) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called with pageable");
        List<ConsumerMeterMappingInterface> consumerMeterMappingInterfaces = null;
        if (consumerNo != null && pageable != null){
            consumerMeterMappingInterfaces = consumerMeterMappingRepository.findByConsumerNo(consumerNo,pageable);
        }
        return consumerMeterMappingInterfaces;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByConsumerNoOrderByInstallationBillMonthDescending(String consumerNo, Pageable pageable) {
        final String methodName = "getByConsumerNoOrderByInstallationBillMonthDescending() : ";
        logger.info(methodName + "called with pageable");
        List<ConsumerMeterMappingInterface> consumerMeterMappingInterfaces = null;
        if (consumerNo != null && pageable != null){
            consumerMeterMappingInterfaces = consumerMeterMappingRepository.findByConsumerNoOrderByInstallationBillMonthDescending(consumerNo,pageable);
        }
        return consumerMeterMappingInterfaces;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByConsumerNoOrderByInstallationBillMonthAscending(String consumerNo, Pageable pageable) {
        final String methodName = "getByConsumerNoOrderByInstallationBillMonthAscending() : ";
        logger.info(methodName + "called with pageable");
        List<ConsumerMeterMappingInterface> consumerMeterMappingInterfaces = null;
        if (consumerNo != null && pageable != null){
            consumerMeterMappingInterfaces = consumerMeterMappingRepository.findByConsumerNoOrderByInstallationBillMonthAscending(consumerNo,pageable);
        }
        return consumerMeterMappingInterfaces;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByMeterIdentifier(String meterIdentifier, Pageable pageable) {
        final String methodName = "getByMeterIdentifier() : ";
        logger.info(methodName + "called with pageable");
        List<ConsumerMeterMappingInterface> consumerMeterMappingInterfaces = null;
        if (meterIdentifier != null && pageable != null){
            consumerMeterMappingInterfaces = consumerMeterMappingRepository.findByMeterIdentifier(meterIdentifier,pageable);
        }
        return consumerMeterMappingInterfaces;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByMeterIdentifierOrderByInstallationBillMonthDescending(String meterIdentifier, Pageable pageable) {
        final String methodName = "getByMeterIdentifier() : ";
        logger.info(methodName + "called with pageable");
        List<ConsumerMeterMappingInterface> consumerMeterMappingInterfaces = null;
        if (meterIdentifier != null && pageable != null){
            consumerMeterMappingInterfaces = consumerMeterMappingRepository.findByMeterIdentifierOrderByInstallationBillMonthDescending(meterIdentifier,pageable);
        }
        return consumerMeterMappingInterfaces;
    }

    @Override
    public List<ConsumerMeterMappingInterface> getByMeterIdentifierOrderByInstallationBillMonthAscending(String meterIdentifier, Pageable pageable) {
        final String methodName = "getByMeterIdentifierOrderByInstallationBillMonthAscending() : ";
        logger.info(methodName + "called with pageable");
        List<ConsumerMeterMappingInterface> consumerMeterMappingInterfaces = null;
        if (meterIdentifier != null && pageable != null){
            consumerMeterMappingInterfaces = consumerMeterMappingRepository.findByMeterIdentifierOrderByInstallationBillMonthAscending(meterIdentifier,pageable);
        }
        return consumerMeterMappingInterfaces;
    }
}
