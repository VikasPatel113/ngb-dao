package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ReadMasterKWDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ReadMasterKWRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterKWInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by RUPALI on 9/16/2017.
 */
@Service
public class ReadMasterKWDAO implements ReadMasterKWDAOInterface<ReadMasterKWInterface> {

    @Autowired
    ReadMasterKWRepository readMasterKWRepository;

    /**
     *
     */
    Logger logger = GlobalResources.getLogger(ReadMasterKWDAO.class);

    @Override
    public ReadMasterKWInterface getByReadMasterId(long readMasterId) {
        final String methodName = "getByReadMasterId() : ";
        logger.info(methodName + "called");
        ReadMasterKWInterface readMasterKW = null;
        if(readMasterId > 0){
            readMasterKW = readMasterKWRepository.findByReadMasterId(readMasterId);
        }
        return readMasterKW;
    }

    @Override
    public ReadMasterKWInterface add(ReadMasterKWInterface readMasterKW) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ReadMasterKWInterface insertedReadMasterKW = null;
        if(readMasterKW != null){
         insertedReadMasterKW = readMasterKWRepository.save(readMasterKW);
        }
        return insertedReadMasterKW;
    }

    @Override
    public ReadMasterKWInterface get(ReadMasterKWInterface readMasterKW) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ReadMasterKWInterface existingReadMasterKW = null;
        if(readMasterKW != null){
            existingReadMasterKW = readMasterKWRepository.findOne(readMasterKW.getId());
        }
        return existingReadMasterKW;
    }

    @Override
    public ReadMasterKWInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ReadMasterKWInterface readMasterKW = null;
        readMasterKW = readMasterKWRepository.findOne(id);
        return readMasterKW;
    }

    @Override
    public ReadMasterKWInterface update(ReadMasterKWInterface readMasterKW) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ReadMasterKWInterface updatedReadMasterKW = null;
        if(readMasterKW != null){
            updatedReadMasterKW = readMasterKWRepository.save(readMasterKW);
        }
        return updatedReadMasterKW;
    }
}
