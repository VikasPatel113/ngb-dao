package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.PowerFactorDAOInterface;
import com.mppkvvcl.ngbdao.repositories.PowerFactorRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PowerFactorInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * Created by ANSHIKA on 16-09-2017.
 */
@Service
public class PowerFactorDAO implements PowerFactorDAOInterface<PowerFactorInterface> {

    /**
     * Requesting spring to get singleton  PowerFactorRepository object.
     */
    @Autowired
    PowerFactorRepository powerFactorRepository;

    /**
     * Getting whole logger object from global resources for current class.
     */
    Logger logger = GlobalResources.getLogger(PowerFactorDAO.class);

    @Override
    public PowerFactorInterface getByPowerFactorValueAndDate(BigDecimal pfValue, Date date) {
        final String methodName = "getByPowerFactorValueAndDate() : ";
        logger.info(methodName + "called");
        PowerFactorInterface powerFactor = null;
        if(pfValue != null && date != null){
            powerFactor = powerFactorRepository.findByPowerFactorValueAndDate(pfValue, date);
        }
        return powerFactor;
    }

    @Override
    public List<? extends PowerFactorInterface> getAll() {
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        List<? extends PowerFactorInterface> powerFactorInterfaces = powerFactorRepository.findAll();
        return powerFactorInterfaces;
    }

    @Override
    public List<PowerFactorInterface> getByDate(Date date) {
        final String methodName = "getByDate() : ";
        logger.info(methodName + "called");
        List<PowerFactorInterface> powerFactorInterfaces = null;
        if(date != null){
            powerFactorInterfaces = powerFactorRepository.findByDate(date);
        }
        return powerFactorInterfaces;
    }

    @Override
    public PowerFactorInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "is called");
        PowerFactorInterface powerFactor = null;
        powerFactor = powerFactorRepository.findOne(id);
        return powerFactor;
    }

    @Override
    public PowerFactorInterface get(PowerFactorInterface powerFactor) {
        final String methodName = "get() : ";
        logger.info(methodName + "is called");
        PowerFactorInterface existingPowerFactor = null;
        if(powerFactor != null){
            powerFactor = powerFactorRepository.findOne(powerFactor.getId());
        }
        return powerFactor;
    }

    @Override
    public PowerFactorInterface add(PowerFactorInterface powerFactor) {
        final String methodName = "add() : ";
        logger.info(methodName + "is called");
        PowerFactorInterface insertedPowerFactor = null;
        if(powerFactor != null){
            powerFactor = powerFactorRepository.save(powerFactor);
        }
        return powerFactor;
    }

    @Override
    public PowerFactorInterface update(PowerFactorInterface powerFactor) {
        final String methodName = "update() : ";
        logger.info(methodName + "is called");
        PowerFactorInterface updatedowerFactor = null;
        if(powerFactor != null){
            powerFactor = powerFactorRepository.save(powerFactor);
        }
        return powerFactor;
    }
}
