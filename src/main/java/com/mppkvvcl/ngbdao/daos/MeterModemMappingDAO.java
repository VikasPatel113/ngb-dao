package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.MeterModemMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.MeterModemMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MeterModemMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
@Service
public class MeterModemMappingDAO implements MeterModemMappingDAOInterface<MeterModemMappingInterface> {

    Logger logger = GlobalResources.getLogger(MeterModemMappingDAO.class);

    @Autowired
    MeterModemMappingRepository meterModemMappingRepository;


    @Override
    public MeterModemMappingInterface add(MeterModemMappingInterface meterModemMapping) {
        final String methodName = "add() : ";
        logger.info(methodName+"clicked");
        MeterModemMappingInterface insertedMeterModemMapping = null;
        if (meterModemMapping != null){
            insertedMeterModemMapping = meterModemMappingRepository.save(meterModemMapping);
        }
        return insertedMeterModemMapping;
    }

    @Override
    public MeterModemMappingInterface update(MeterModemMappingInterface meterModemMapping) {
        final String methodName = "update() : ";
        logger.info(methodName+"clicked");
        MeterModemMappingInterface getMeterModemMapping = null;
        if (meterModemMapping != null){
            getMeterModemMapping = meterModemMappingRepository.save(meterModemMapping);
        }
        return getMeterModemMapping;
    }

    @Override
    public MeterModemMappingInterface get(MeterModemMappingInterface meterModemMapping) {
        final String methodName = "get() : ";
        logger.info(methodName+"clicked");
        MeterModemMappingInterface getMeterModemMapping = null;
        if (meterModemMapping != null){
            getMeterModemMapping = meterModemMappingRepository.findOne(meterModemMapping.getId());
        }
        return getMeterModemMapping;
    }

    @Override
    public MeterModemMappingInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName+"clicked");
        MeterModemMappingInterface meterModemMapping = null;
        if (id > 0){
            meterModemMapping = meterModemMappingRepository.findOne(id);
        }
        return meterModemMapping;
    }

    @Override
    public List<MeterModemMappingInterface> getByMeterIdentifier(String meterIdentifier) {
        final String methodName = "getByMeterIdentifier() :  ";
        logger.info(methodName+"clicked");
        List<MeterModemMappingInterface> meterModemMapping = null;
        if (meterIdentifier != null){
            meterModemMapping  = meterModemMappingRepository.findByMeterIdentifier(meterIdentifier);
        }
        return meterModemMapping;
    }

    @Override
    public List<MeterModemMappingInterface> getByMeterIdentifierAndStatus(String meterIdentifier, String status) {
        final String methodName = "getByMeterIdentifierAndStatus() :  ";
        logger.info(methodName+"clicked");
        List<MeterModemMappingInterface> meterModemMappings =  null;
        if (meterIdentifier != null && status != null){
            meterModemMappings = meterModemMappingRepository.findByMeterIdentifierAndStatus(meterIdentifier , status);
        }
        return meterModemMappings;
    }
}
