package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.TariffChangeDetailDAOInterface;
import com.mppkvvcl.ngbdao.repositories.TariffChangeDetailRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.TariffChangeDetailInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by vikas on 10/6/2017.
 */

@Service
public class TariffChangeDetailDAO implements TariffChangeDetailDAOInterface<TariffChangeDetailInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(TariffChangeDetailDAO.class);

    @Autowired
    private TariffChangeDetailRepository tariffChangeDetailRepository = null;

    @Override
    public TariffChangeDetailInterface add(TariffChangeDetailInterface tariffChangeDetail) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        TariffChangeDetailInterface insertedTariffChangeDetail = null;
        if (tariffChangeDetail != null){
            insertedTariffChangeDetail = tariffChangeDetailRepository.save(tariffChangeDetail);
        }
        return insertedTariffChangeDetail;
    }


    @Override
    public TariffChangeDetailInterface update(TariffChangeDetailInterface tariffChangeDetail) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        TariffChangeDetailInterface updatedTariffChangeDetail = null;
        if (tariffChangeDetail != null){
            updatedTariffChangeDetail = tariffChangeDetailRepository.save(tariffChangeDetail);
        }
        return updatedTariffChangeDetail;
    }

    @Override
    public TariffChangeDetailInterface get(TariffChangeDetailInterface tariffChangeDetail) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        TariffChangeDetailInterface existingTariffDetail = null;
        if (tariffChangeDetail != null){
            existingTariffDetail = tariffChangeDetailRepository.findOne(tariffChangeDetail.getId());
        }
        return existingTariffDetail;
    }

    @Override
    public TariffChangeDetailInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        TariffChangeDetailInterface existingTariffDetail = null;
        if(id != 0){
            existingTariffDetail = tariffChangeDetailRepository.findOne(id);
        }
        return existingTariffDetail;
    }

    @Override
    public TariffChangeDetailInterface getByTariffDetailId(long tariffDetailId) {
        final String methodName = "getByTariffDetailId() : ";
        logger.info(methodName + "called");
        TariffChangeDetailInterface existingTariffDetail = null;
        if(tariffDetailId != 0){
            existingTariffDetail = tariffChangeDetailRepository.findByTariffDetailId(tariffDetailId);
        }
        return existingTariffDetail;
    }
}
