package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ConsumerConnectionInformationHistoryDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ConsumerConnectionInformationHistoryRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionInformationHistoryInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
@Service
public class ConsumerConnectionInformationHistoryDAO implements ConsumerConnectionInformationHistoryDAOInterface<ConsumerConnectionInformationHistoryInterface> {
    @Autowired
    private ConsumerConnectionInformationHistoryRepository consumerConnectionInformationHistoryRepository;
    /**
     *
     */
    private Logger logger = GlobalResources.getLogger(ConsumerConnectionInformationHistoryDAO.class);

    @Override
    public List<ConsumerConnectionInformationHistoryInterface> getByConsumerNoAndPropertyNameAndEndBillMonth(String consumerNo, String propertyName, String endMonth) {
        final String methodName = "getByConsumerNoAndPropertyNameAndEndBillMonth() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionInformationHistoryInterface> consumerConnectionInformationHistories = null;
        if(consumerNo != null && propertyName != null && endMonth != null){
            consumerConnectionInformationHistories = consumerConnectionInformationHistoryRepository.findByConsumerNoAndPropertyNameAndEndBillMonth(consumerNo, propertyName, endMonth);
        }
        return consumerConnectionInformationHistories;
    }

    @Override
    public List<ConsumerConnectionInformationHistoryInterface> getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionInformationHistoryInterface> consumerConnectionInformationHistories = null;
        if(consumerNo != null){
            consumerConnectionInformationHistories = consumerConnectionInformationHistoryRepository.findByConsumerNo(consumerNo);
        }
        return consumerConnectionInformationHistories;
    }

    @Override
    public List<ConsumerConnectionInformationHistoryInterface> getByConsumerNoAndEndBillMonth(String consumerNo, String endMonth) {
        final String methodName = "getByConsumerNoAndEndBillMonth() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionInformationHistoryInterface> consumerConnectionInformationHistories = null;
        if(consumerNo != null && endMonth != null){
            consumerConnectionInformationHistories = consumerConnectionInformationHistoryRepository.findByConsumerNoAndEndBillMonth(consumerNo, endMonth);
        }
        return consumerConnectionInformationHistories;
    }

    @Override
    public List<ConsumerConnectionInformationHistoryInterface> getByConsumerNoAndPropertyName(String consumerNo, String propertyName) {
        final String methodName = "getByConsumerNoAndPropertyName() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionInformationHistoryInterface> consumerConnectionInformationHistories = null;
        if(consumerNo != null && propertyName != null){
            consumerConnectionInformationHistories = consumerConnectionInformationHistoryRepository.findByConsumerNoAndPropertyName(consumerNo, propertyName);
        }
        return consumerConnectionInformationHistories;
    }

    @Override
    public List<ConsumerConnectionInformationHistoryInterface> getByConsumerNoAndPropertyNameAndEndDateBetween(String consumerNo, String propertyName, Date startDate, Date endDate) {
        final String methodName = "getByConsumerNoAndPropertyNameAndEndDateBetween() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionInformationHistoryInterface> consumerConnectionInformationHistories = null;
        if(consumerNo != null && propertyName != null && startDate != null && endDate != null){
            consumerConnectionInformationHistories = consumerConnectionInformationHistoryRepository.findByConsumerNoAndPropertyNameAndEndDateBetween(consumerNo, propertyName, startDate, endDate);
        }
        return consumerConnectionInformationHistories;
    }

    @Override
    public ConsumerConnectionInformationHistoryInterface add(ConsumerConnectionInformationHistoryInterface consumerConnectionInformationHistory) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ConsumerConnectionInformationHistoryInterface insertedConsumerConnectionInformationHistory = null;
        if(consumerConnectionInformationHistory != null){
            insertedConsumerConnectionInformationHistory = consumerConnectionInformationHistoryRepository.save(consumerConnectionInformationHistory);
        }
        return insertedConsumerConnectionInformationHistory;
    }

    @Override
    public ConsumerConnectionInformationHistoryInterface get(ConsumerConnectionInformationHistoryInterface consumerConnectionInformationHistory) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ConsumerConnectionInformationHistoryInterface existingConsumerConnectionInformationHistory = null;
        if(consumerConnectionInformationHistory != null){
            existingConsumerConnectionInformationHistory = consumerConnectionInformationHistoryRepository.findOne(consumerConnectionInformationHistory.getId());
        }
        return existingConsumerConnectionInformationHistory;
    }

    @Override
    public ConsumerConnectionInformationHistoryInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ConsumerConnectionInformationHistoryInterface consumerConnectionInformationHistory = null;
        consumerConnectionInformationHistory = consumerConnectionInformationHistoryRepository.findOne(id);
        return consumerConnectionInformationHistory;
    }

    @Override
    public ConsumerConnectionInformationHistoryInterface update(ConsumerConnectionInformationHistoryInterface consumerConnectionInformationHistory) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ConsumerConnectionInformationHistoryInterface updatedConsumerConnectionInformationHistory = null;
        if(consumerConnectionInformationHistory != null){
            updatedConsumerConnectionInformationHistory = consumerConnectionInformationHistoryRepository.save(consumerConnectionInformationHistory);
        }
        return updatedConsumerConnectionInformationHistory;
    }
}
