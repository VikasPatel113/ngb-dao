package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ReadMasterPFDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ReadMasterPFRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ReadMasterPFInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by RUPALI on 9/16/2017.
 */
@Service
public class ReadMasterPFDAO implements ReadMasterPFDAOInterface<ReadMasterPFInterface> {

    @Autowired
    ReadMasterPFRepository readMasterPFRepository;

    Logger logger = GlobalResources.getLogger(ReadMasterPFDAO.class);

    @Override
    public ReadMasterPFInterface getByReadMasterId(Long readMasterId) {
        final String methodName = "getByReadMasterId() : ";
        logger.info(methodName + " called");
        ReadMasterPFInterface readMasterPF = null;
        if(readMasterId > 0){
            readMasterPF = readMasterPFRepository.findByReadMasterId(readMasterId);
        }
        return readMasterPF;
    }

    @Override
    public ReadMasterPFInterface add(ReadMasterPFInterface readMasterPF) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ReadMasterPFInterface insertedReadMasterPF = null;
        if(readMasterPF != null){
            insertedReadMasterPF = readMasterPFRepository.save(readMasterPF);
        }
        return insertedReadMasterPF;
    }

    @Override
    public ReadMasterPFInterface get(ReadMasterPFInterface readMasterPF) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ReadMasterPFInterface existingReadMasterPF = null;
        if(readMasterPF != null){
            existingReadMasterPF = readMasterPFRepository.findOne(readMasterPF.getId());
        }
        return existingReadMasterPF;
    }

    @Override
    public ReadMasterPFInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ReadMasterPFInterface readMasterPF = null;
        readMasterPF = readMasterPFRepository.findOne(id);
        return readMasterPF;
    }

    @Override
    public ReadMasterPFInterface update(ReadMasterPFInterface readMasterPF) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ReadMasterPFInterface updatedReadMasterPF = null;
        if(readMasterPF != null){
            updatedReadMasterPF = readMasterPFRepository.save(readMasterPF);
        }
        return updatedReadMasterPF;
    }
}
