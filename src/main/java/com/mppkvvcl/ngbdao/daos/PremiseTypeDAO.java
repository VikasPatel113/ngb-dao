package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.PremiseTypeDAOInterface;
import com.mppkvvcl.ngbdao.repositories.PremiseTypeRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PremiseTypeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
@Service
public class PremiseTypeDAO implements PremiseTypeDAOInterface<PremiseTypeInterface> {

    /**
     *
     */
    private Logger logger = GlobalResources.getLogger(PremiseTypeDAO.class);

    @Autowired
    private PremiseTypeRepository premiseTypeRepository;

    @Override
    public PremiseTypeInterface add(PremiseTypeInterface premiseType) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        PremiseTypeInterface insertedPremiseType = null;
        if(premiseType != null){
            insertedPremiseType = premiseTypeRepository.save(premiseType);
        }
        return insertedPremiseType;
    }

    @Override
    public PremiseTypeInterface get(PremiseTypeInterface premiseType) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        PremiseTypeInterface existingPremiseType = null;
        if(premiseType != null){
            existingPremiseType = premiseTypeRepository.findOne(premiseType.getId());
        }
        return existingPremiseType;
    }

    @Override
    public PremiseTypeInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + " called");
        PremiseTypeInterface premiseType = null;
        premiseType = premiseTypeRepository.findOne(id);
        return premiseType;
    }

    @Override
    public PremiseTypeInterface update(PremiseTypeInterface premiseType) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        PremiseTypeInterface updatedPremiseType = null;
        if(premiseType != null){
            updatedPremiseType = premiseTypeRepository.save(premiseType);
        }
        return updatedPremiseType;
    }

    public List<? extends PremiseTypeInterface> getAll() {
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        List<? extends PremiseTypeInterface> existingPremiseTypes = premiseTypeRepository.findAll();
        return existingPremiseTypes;
    }
}
