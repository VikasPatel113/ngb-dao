package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.EmployeeMasterDAOInterface;
import com.mppkvvcl.ngbdao.repositories.EmployeeMasterRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.EmployeeMasterInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class EmployeeMasterDAO implements EmployeeMasterDAOInterface<EmployeeMasterInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(EmployeeMasterDAO.class);

    @Autowired
    EmployeeMasterRepository employeeMasterRepository = null;

    @Override
    public EmployeeMasterInterface add(EmployeeMasterInterface employeeMaster) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        EmployeeMasterInterface insertedEmployeeMaster = null;
        if (employeeMaster != null){
            insertedEmployeeMaster = employeeMasterRepository.save(employeeMaster);
        }
        return insertedEmployeeMaster;
    }

    @Override
    public EmployeeMasterInterface update(EmployeeMasterInterface employeeMaster) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        EmployeeMasterInterface updatedEmployeeMaster = null;
        if (employeeMaster != null){
            updatedEmployeeMaster = employeeMasterRepository.save(employeeMaster);
        }
        return updatedEmployeeMaster;
    }

    @Override
    public EmployeeMasterInterface get(EmployeeMasterInterface employeeMaster) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        EmployeeMasterInterface existingEmployeeMaster = null;
        if (employeeMaster != null){
            existingEmployeeMaster = employeeMasterRepository.findOne(employeeMaster.getId());
        }
        return existingEmployeeMaster;
    }

    @Override
    public EmployeeMasterInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        EmployeeMasterInterface existingEmployeeMaster = null;
        if(id != 0){
            existingEmployeeMaster = employeeMasterRepository.findOne(id);
        }
        return existingEmployeeMaster;
    }

    @Override
    public EmployeeMasterInterface getByEmployeeNo(String employeeNo) {
        final String methodName = "getByEmployeeNo() : ";
        logger.info(methodName + "called");
        EmployeeMasterInterface fetchedEmployeeMaster = null;
        if(employeeNo != null){
            fetchedEmployeeMaster = employeeMasterRepository.findByEmployeeNo(employeeNo);
        }
        return fetchedEmployeeMaster;
    }

    @Override
    public List<EmployeeMasterInterface> getByCompany(String company) {
        final String methodName = "getByCompany() : ";
        logger.info(methodName + "called");
        List<EmployeeMasterInterface> fetchedEmployeeMaster = null;
        if(company != null){
            fetchedEmployeeMaster = employeeMasterRepository.findByCompany(company);
        }
        return fetchedEmployeeMaster;
    }

    @Override
    public EmployeeMasterInterface getByEmployeeNoAndStatus(String employeeNo, String status) {
        final String methodName = "getByEmployeeNoAndStatus() : ";
        logger.info(methodName + "called");
        EmployeeMasterInterface fetchedEmployeeMaster = null;
        if(employeeNo != null && status != null){
            fetchedEmployeeMaster = employeeMasterRepository.findByEmployeeNoAndStatus(employeeNo , status);
        }
        return fetchedEmployeeMaster;
    }

    @Override
    public EmployeeMasterInterface getByEmployeeNoAndTypeAndStatus(String employeeNo, String type, String status) {
        final String methodName = "getByEmployeeNoAndConsumerNoAndStatus() : ";
        logger.info(methodName + "called");
        EmployeeMasterInterface fetchedEmployeeConnectionMapping = null;
        if(employeeNo != null && type != null && status != null){
            fetchedEmployeeConnectionMapping = employeeMasterRepository.findByEmployeeNoAndTypeAndStatus(employeeNo , type , status);
        }
        return fetchedEmployeeConnectionMapping;
    }
}
