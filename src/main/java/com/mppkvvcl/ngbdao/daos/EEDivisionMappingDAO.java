package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.EEDivisionMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.EEDivisionMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.EEDivisionMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EEDivisionMappingDAO implements EEDivisionMappingDAOInterface<EEDivisionMappingInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private static final Logger logger = GlobalResources.getLogger(EEDivisionMappingDAO.class);

    @Autowired
    private EEDivisionMappingRepository eeDivisionMappingRepository;

    @Override
    public EEDivisionMappingInterface getByUsername(final String username) {
        final String methodName = "getByUsername() : ";
        logger.info(methodName + "called with username " + username);
        EEDivisionMappingInterface eeDivisionMapping = null;
        if(username != null){
            eeDivisionMapping = eeDivisionMappingRepository.findByUserDetailUsername(username);
        }
        return eeDivisionMapping;
    }

    @Override
    public EEDivisionMappingInterface getByDivisionId(final long divisionId) {
        final String methodName = "getByDivisionId() : ";
        logger.info(methodName + "called with division id " + divisionId);
        EEDivisionMappingInterface eeDivisionMapping = null;
        eeDivisionMapping = eeDivisionMappingRepository.findByDivisionId(divisionId);
        return eeDivisionMapping;
    }

    @Override
    public EEDivisionMappingInterface add(EEDivisionMappingInterface eeDivisionMapping) {
        return null;
    }

    @Override
    public EEDivisionMappingInterface update(EEDivisionMappingInterface eeDivisionMapping) {
        return null;
    }

    @Override
    public EEDivisionMappingInterface get(EEDivisionMappingInterface eeDivisionMapping) {
        return null;
    }

    @Override
    public EEDivisionMappingInterface getById(long id) {
        return null;
    }
}
